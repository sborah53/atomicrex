res
set term postscript eps enh col "Helvetica" 22
set out "eps/screening_functions.eps"
set enc iso_8859_1

#--------------------------------------------------------
col_red    = "#D7191C"
col_orange = "#FDAE61"
col_yellow = "#FFFFBF"
col_blue1  = "#ABD9E9"
col_blue2  = "#2C7BB6"
#------------
set style incr user
LW=6
LS=0
PS = 2.4
LS=LS+1 ; set style line LS lt 2 pt  7 ps PS lw LW lc rgb col_orange
LS=LS+1 ; set style line LS lt 1 pt  5 ps PS lw LW lc rgb col_red
LS=LS+1 ; set style line LS lt 3 pt  8 ps 1.2*PS lw LW lc rgb col_blue2
LS=LS+1 ; set style line LS lt 4 pt 12 ps 1.2*PS lw LW lc rgb col_blue1
LS=10
LS=LS+1 ; set style line LS lt 1 lw 8 lc rgb "#bbbbbb"
LS=LS+1 ; set style line LS lt 2 lw 8 lc rgb "#bbbbbb"
#----
LW=4
LS=10
set style arr 11 lt 1 lw LW lc 0 filled head
set style arr 12 lt 1 lw LW lc 0 filled heads
set style arr 13 lt 3 lw LW lc 0 nohead

#--------------------------------------------------------
set xla '{/=24 Argument {/Helvetica-Oblique r}/{/Helvetica-Oblique r_c}}'
rc = 1.0
set xra [0:1.0*rc]
set form x '%.1f'
#----
set yla '{/=24 Screening function {/Helvetica-Oblique f}({/Helvetica-Oblique r})}'
set form y '%.1f'
set yra [0:1.05]
#----
set key Left rev spac 1.4 noautoti
set key wid -10
set key above

ff1(r) = exp( 1.0 / ( r - rc) )
f1(r) = ff1(r) / ff1(0)
ff2(r) = exp( -sgn(n) * alpha / ( 1.0-((r-rc1)/(rc-rc1))**n ) )
f2(r) = r > rc1 ? ff2(r) / ff2(rc1) : 1.0
ff3(r) = f1(r) * 1.0 / 2 / sqrt(2.*pi) * exp( - r**2 / 2 / sigma**2 )
f3(r) = ff3(r) / ff3(0)

str1 = 'Inverse exponential'
str2 = 'Expanded inverse exponential: {/Helvetica-Oblique n} = %.0f, {/Helvetica-Oblique r_c}^{({/Helvetica-Oblique i})} = %.1f {/Helvetica-Oblique r_c}, {/Symbol a} = %.1f'
str3 = 'Gaussian with cutoff: {/Helvetica-Oblique n} = %.0f, {/Helvetica-Oblique r_c}^{({/Helvetica-Oblique i})} = %.1f, {/Symbol a} = %.1f, {/Symbol s} = %.1f'

p \
  f1(x) t sprintf(str1), \
  alpha=0.2, rc1=0.2, n=1.0, f2(x) t sprintf(str2, n, rc1, alpha), \
  alpha=0.2, rc1=0.0, sigma=0.8, n=3.0, f3(x) t sprintf(str3, n, rc1, alpha, sigma), \
  alpha=0.4, rc1=0.0, sigma=0.3, n=1.0, f3(x) t sprintf(str3, n, rc1, alpha, sigma)