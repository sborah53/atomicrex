.. _python_interface:

.. index::
   single: Python interface
   single: Python interface; overview

	       
Python interface
*****************

:program:`atomicrex` provides a Python interface. This enables
integration with countless Python libraries, which can be used to
employ alternative optimization algorithms (e.g., via `scipy
<https://www.scipy.org/>`_), hook up to general machine learning
schemes (e.g., via `scikit-learn <http://scikit-learn.org/>`_), or
implement simple parallelization schemes. Additionally :program:`atomicrex`
can be used as a calculator in `ASE <https://wiki.fysik.dtu.dk/ase/index.html>`_.

.. rubric:: Contents

.. toctree::
   :glob:
   :maxdepth: 2

   example_adding_structures_library
   example_adding_structures_aseatoms
   example_basic_fitting
   example_ase_calculator
   moduleref
