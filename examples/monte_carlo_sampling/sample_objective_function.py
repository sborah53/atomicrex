#!/usr/bin/env python3

"""
This script enable sampling of the parameter space of a potential using
Monte Carlo (MC) simulations. It can also be used to conduct parameter
optimization via simulated annealing.
"""

from __future__ import print_function
import atomicrex
import random
import numpy as np
import argparse

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument('temperature_initial',
                    type=float,
                    help='Initial temperature')
defval = None
parser.add_argument('-f', '--temperature_final',
                    default=defval,
                    type=float, metavar='T',
                    help='Final temperature'
                    ' [default: identical to initial temperature]')
defval = 400000
parser.add_argument('-n', dest='number_of_steps',
                    type=int, metavar='N',
                    default=defval,
                    help='Number of MC trial steps'
                    ' [default: {}]'.format(defval))
defval = 0.2
parser.add_argument('-t', dest='target_acceptance_probability',
                    type=float, metavar='phi',
                    default=defval,
                    help='Target acceptance probability for trial step'
                    ' optimization, see `<https://arxiv.org/abs/1211.4440>`_'
                    ' [default: {}]'.format(defval))
defval = 300
parser.add_argument('-w', dest='sampling_window',
                    type=int, metavar='N',
                    default=defval,
                    help='Number of MC trial steps over which acceptance'
                    ' probability is sampled [default: {}]'.format(defval))
defval = 100
parser.add_argument('-o', dest='output_spacing',
                    type=int, metavar='N',
                    default=defval,
                    help='Number of MC trial steps between output'
                    ' [default: {}]'.format(defval))
defval = 15611
parser.add_argument('-s', dest='seed',
                    type=int, metavar='N',
                    default=defval,
                    help='Seed for random number generator'
                    ' [default: {}]'.format(defval))
args = parser.parse_args()

if args.temperature_final is None:
    args.temperature_final = args.temperature_initial

print('Arguments:')
for arg in args.__dict__:
    print(' {:32}: {}'.format(arg, args.__dict__[arg]))
print('')

if args.temperature_final <= 0.0:
    print('Final temperature must be positive.')
    exit(1)

# set up job
print('Optimize parameters (without "temperature")')
job = atomicrex.Job()
job.parse_input_file('main.xml')
job.prepare_fitting()
print

# output initial parameters
print('Initial residual: {}'.format(job.calculate_residual()))
print('Initial parameters:')
print('%-14s' % 'parameters:')
for p in job.get_potential_parameters():
    print(' {:10}'.format(p), end='')
print('')
print('{:14}'.format('1st derivative:'))
grad = job.calculate_gradient()
for g in grad:
    print('{:10}'.format(g), end='')
print('')
print('{:14}'.format('2nd derivative:'))
hessian = job.calculate_hessian()
for row in hessian:
    for d in row:
        print('{:10}'.format(d), end='')
    print('')
print
print

# output properties
for name, structure in sorted(job.structures.items()):
    print
    print(' Structure: {}'.format(name))
    structure.compute_properties()
    structure.print_properties()
print

# set up Monte Carlo
print('trial step lengths:'),
trial_step_lengths = 1.0 / np.diag(hessian)
for s in trial_step_lengths:
    print(' {:12}'.format(s), end='')
print('')
random.seed(args.seed)

# run Monte Carlo
print('Monte Carlo sampling of parameter space:')
params = job.get_potential_parameters()
chi = job.calculate_residual()
number_of_rejected_trial_steps = 0
events = []
for p in params:
    events.append([])
temperature_delta = args.temperature_final - args.temperature_initial
temperature_delta /= args.number_of_steps
for it in range(args.number_of_steps + 1):

    temperature = args.temperature_initial + temperature_delta * it

    # trial step
    ip = random.randrange(len(params))
    delta = trial_step_lengths[ip] * random.uniform(-1, 1)
    params[ip] += delta
    chi_new = job.calculate_residual(params)
    events[ip].append(1)
    if len(events[ip]) > args.sampling_window:
        events[ip].pop(0)
    if chi_new < chi:
        chi = chi_new
        accprob = 1.0
    else:
        accprob = np.exp((chi - chi_new) / temperature)
        if accprob > random.uniform(0, 1):
            chi = chi_new
        else:
            number_of_rejected_trial_steps += 1
            params[ip] -= delta
            events[ip][-1] = 0

    # output and basic analysis
    if it % args.output_spacing != 0:
        continue

    print('mc', end='')
    print(' {:8}'.format(it), end='')
    print(' {:12.8}'.format(temperature), end='')
    print(' {:12.8g}'.format(chi), end='')

    print('  ', end='')
    for p in params:
        print(' {:12.8g}'.format(p), end='')

    print('  ', end='')
    for k, e in enumerate(events):
        try:
            prob = float(np.sum(e)) / len(e)
        except:
            prob = 0.0
        print(' {:9.5}'.format(prob), end='')
        print(' ( {:9.5})'.format(trial_step_lengths[k]), end='')

        # adapt trial step lengths
        if k == ip:
            if prob > 0:
                trial_step_lengths[ip] *= prob
                trial_step_lengths[ip] /= args.target_acceptance_probability
            else:
                trial_step_lengths[ip] *= 0.9

    s = job.structures['FCC']
    s.compute_properties()
    print(' {:9.3}'.format(s.properties['atomic-energy'].computed_value),
          end='')
    print(' {:7.3}'.format(s.properties['lattice-parameter'].computed_value),
          end='')
    print(' {:9.2}'.format(s.properties['C11'].computed_value), end='')
    print(' {:9.2}'.format(s.properties['C12'].computed_value), end='')
    print(' {:9.2}'.format(s.properties['C44'].computed_value), end='')

    print('')
