from __future__ import print_function, division
import atomicrex
from scipy.optimize import minimize

__doc__ = """This script tests the application of the Python interface
for parameter optimization.  """

job = atomicrex.Job()
job.parse_input_file('main.xml')
job.set_verbosity(2)

structure = job.add_library_structure('test-fcc',
                                      'fcc',
                                      {'alat': 4.032, 'type': 'Cu'})
structure.modify_property('atomic-energy', -4.6, 1.0)
structure.modify_property('lattice-parameter', 3.7, 0.10)

print('\nFit parameters using the internal l-BFGS minimizer.')
job.prepare_fitting()
job.set_potential_parameters([2.5, 0.5])
res = job.calculate_residual()
job.perform_fitting()
print(' residual: {}'.format(job.calculate_residual()))

print('\nFit parameters using scipys l-BFGS minimizer.')
resopt = minimize(job.calculate_residual,
                  [2.5, 0.5],
                  options={'disp': False,
                           'eps': 1e-4,
                           'gtol': 1e-10,
                           'maxiter': 10},
                  method='L-BFGS-B')
print(' residual: {}'.format(job.calculate_residual()))
job.print_potential_parameters()
