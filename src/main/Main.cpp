///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include <arexlib/Atomicrex.h>
#include <arexlib/job/FitJob.h>
#include <arexlib/structures/AtomicStructure.h>
#include <arexlib/potentials/Potential.h>

#include <locale>

using namespace std;
using namespace atomicrex;

void printHelp()
{
	cerr << "Atomicrex (version " << ATOMICREX_VERSION_STRING << ")" << endl;
	cerr << "Authors: " << endl;
	cerr << "         Alexander Stukowski <stukowski@mm.tu-darmstadt.de>" << endl;
	cerr << "         Paul Erhart         <erhart@chalmers.se>" << endl;
	cerr << "Contributors: " << endl;
	cerr << "         Markus Mock         <mock@mm.tu-darmstadt.de>" << endl;
	cerr << "         Mattias Ångqvist    " << endl;
	cerr << "         Erik Fransson       " << endl;
	cerr << "License: GNU General Public License (v3), see LICENSE.txt" << endl;
	cerr << endl;
	cerr << "Usage: atomicrex [options] inputfile" << endl;
}

int main(int argc, char* argv[])
{
	// The following sets the locale to the standard "C" locale, which is independent of the user's system settings.
	locale::global(locale::classic());

	// Check command line arguments.
	if(argc != 2) {
		printHelp();
		return 1;
	}

	FPString jobFilename = argv[1];

	try {
		MsgLogger(medium) << "This is program version " << ATOMICREX_VERSION_STRING << endl;
		MsgLogger(medium) << "Reading job file " << jobFilename << endl;
		MsgLogger(medium) << separatorLine;

		// Parse XML document tree.
		MsgLogger(medium) << "Parsing input file(s)" << endl;
		MsgLogger(medium) << separatorLine;
		FitJob job;
		job.parse(jobFilename);

		// Perform a check of the energy/force calculation routines using numerical differentiation.
		if(job.potentialValidationEnabled())
			job.validatePotentials();

		// Perform fitting if requested by the user.
		if(job.fittingEnabled())
		  job.performFitting();

		// Prepare all potentials for following property calculation.
		for(Potential* potential : job.potentials())
			potential->preparePotential();

		// Compute output properties of all structures.
		MsgLogger(medium) << separatorLine;
		MsgLogger(medium) << "Computing structure properties" << endl;
		for(AtomicStructure* structure : job.structures()) {

			// Check if structure has any properties which have been enabled for output.
			bool isOutputEnabled = false;
			for(FitProperty* prop : structure->properties())
				if(prop->outputEnabled())
					isOutputEnabled = true;
			if(!isOutputEnabled) continue;

			// Compute property values.
			structure->computeProperties(false);

			// Print to console.
			MsgLogger(medium) << "Structure '" << structure->id() << "':" << endl;
			for(FitProperty* prop : structure->properties()) {
				if(prop->outputEnabled()) {
					prop->compute();
					MsgLogger logger(medium);
					logger << "   ";
					prop->print(logger);
					logger << endl;
				}
			}

            // Write to file if filenames have been set.
            structure->writeToFile();
		}

        // Compute output of derived properties.
        if (job.derivedProperties().size() > 0) {
            MsgLogger(medium) << separatorLine;
            MsgLogger(medium) << "Computing derived properties" << endl;
            for(DerivedProperty* prop : job.derivedProperties()) {
                if(!prop->outputEnabled()) continue;
                // Print to console.
                prop->compute();
                MsgLogger logger(medium);
                logger << "   ";
                prop->print(logger);
                logger << endl;
            }
        }

		// Output fitting results to files.
		MsgLogger(medium) << separatorLine;
		for(Potential* potential : job.potentials())
			potential->outputResults();
		MsgLogger(medium) << separatorLine;
	}
	catch(const exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	// Done
	return 0;
}
