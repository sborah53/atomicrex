///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "CDIPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

// Data structures to store intermediate results; used by the CDIP force/energy calculation routines.
struct PerAtomAndTypeInfo {
    double sigma_bar;
    Vector3 sigma_bar_grad;
    double sigma_bar_S;
    Vector3 sigma_bar_S_grad;
};

struct PerAtomAndInteractionInfo {
    double M;
    double N;
};

/******************************************************************************
 * Computes the total energy of the structure.
 ******************************************************************************/
double CDIPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;
    int inum = data.neighborList.numAtoms();
    int ntypes = job()->numAtomTypes();
    int ninteractions = _pairInteractions.size();
    BOOST_ASSERT(inum == structure.numLocalAtoms());

    // Allocate data arrays.
    vector<PerAtomAndTypeInfo> perAtomAndTypeArray(inum * ntypes);

    // Reset density arrays to zero.
    for(vector<PerAtomAndTypeInfo>::iterator i = perAtomAndTypeArray.begin(); i != perAtomAndTypeArray.end(); ++i)
        memset(&*i, 0, sizeof(PerAtomAndTypeInfo));

    // Calculate sigma_bar and sigma_bar_S.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        BOOST_ASSERT(i < structure.numLocalAtoms());
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij >= _cutoff) continue;

            int j = neighbor_j->index;
            int jlocal = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            BOOST_ASSERT(i * ntypes < perAtomAndTypeArray.size());
            BOOST_ASSERT(jlocal * ntypes < perAtomAndTypeArray.size());
            vector<PerAtomAndTypeInfo>::iterator atomInfo_i = perAtomAndTypeArray.begin() + (i * ntypes);
            vector<PerAtomAndTypeInfo>::iterator atomInfo_j = perAtomAndTypeArray.begin() + (jlocal * ntypes);
            for(int t = 0; t < ntypes; t++, ++atomInfo_i, ++atomInfo_j) {
                BOOST_ASSERT(t < _sigmas.size());
                BOOST_ASSERT(_sigmas[t] != NULL);
                double s = _sigmas[t]->evaluate(rij);
                atomInfo_i->sigma_bar += s;
                if(!_legacyMode) {
                    atomInfo_j->sigma_bar += s;
                    if(jtype == t) atomInfo_i->sigma_bar_S += s;
                    if(itype == t) atomInfo_j->sigma_bar_S += s;
                }
                else {
                    if(jtype == t) atomInfo_i->sigma_bar_S += s;
                    atomInfo_j->sigma_bar += s;
                    if(itype == t) atomInfo_j->sigma_bar_S += s;
                }
            }
        }
    }

    // Compute pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij >= _cutoff) continue;

            int j = neighbor_j->index;
            int jlocal = neighbor_j->localIndex;
            int jtype = structure.atomType(j);

            BOOST_ASSERT(jtype == structure.atomType(jlocal));
            for(int pi = 0; pi < ninteractions; pi++) {
                const CDIPairInteraction& interaction = *_pairInteractions[pi];
                if(interaction.interacting(itype, jtype)) {
                    double sigma_ij = _sigmas[interaction.matrix()]->evaluate(rij);

                    if(!_legacyMode) {
                        // Compute x_i(j)^S
                        const PerAtomAndTypeInfo& atomTypeInfo_i = perAtomAndTypeArray[i * ntypes + interaction.matrix()];
                        double sigma_bar_ij_S = atomTypeInfo_i.sigma_bar_S;
                        if(jtype == interaction.matrix()) sigma_bar_ij_S -= sigma_ij;
                        double sigma_bar_ij = (atomTypeInfo_i.sigma_bar - sigma_ij);
                        double x_ij_S = 1.0 - sigma_bar_ij_S / sigma_bar_ij;

                        // Compute x_j(i)^S
                        const PerAtomAndTypeInfo& atomTypeInfo_j = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()];
                        double sigma_bar_ji_S = atomTypeInfo_j.sigma_bar_S;
                        if(itype == interaction.matrix()) sigma_bar_ji_S -= sigma_ij;
                        double sigma_bar_ji = (atomTypeInfo_j.sigma_bar - sigma_ij);
                        double x_ji_S = 1.0 - sigma_bar_ji_S / sigma_bar_ji;

                        double h_AB;
                        if(_linearizedMode) {
                            // Evaluate h(x_i(j))
                            double h_ij = interaction.h()->evaluate(x_ij_S);
                            // Evaluate h(x_j(i))
                            double h_ji = interaction.h()->evaluate(x_ji_S);
                            // Compute final h by linear interpolation.
                            h_AB = 0.5 * (h_ij + h_ji);
                        }
                        else {
                            double xij = 1.0 - 0.5 * (x_ij_S + x_ji_S);
                            // Evaluate h(xij).
                            h_AB = interaction.h()->evaluate(xij);
                        }

                        double V_AB = interaction.V()->evaluate(rij);
                        totalEnergy += h_AB * V_AB;
                    }
                    else {
                        double h_AB;

                        // Compute x_i^S
                        double sigma_bar_i_S = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar_S;
                        double sigma_bar_i = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar;
                        double x_i = 1.0 - sigma_bar_i_S / sigma_bar_i;

                        // Compute x_j^S
                        double sigma_bar_j_S = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar_S;
                        double sigma_bar_j = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar;
                        double x_j = 1.0 - sigma_bar_j_S / sigma_bar_j;

                        // Evaluate h(xij).
                        if(_linearizedMode) {
                            // Evaluate h(x_i)
                            double h_i = interaction.h()->evaluate(x_i);
                            // Evaluate h(x_j)
                            double h_j = interaction.h()->evaluate(x_j);
                            // Compute final h by linear interpolation.
                            h_AB = 0.5 * (h_i + h_j);
                        }
                        else {
                            double xij = 0.5 * (x_i + x_j);
                            h_AB = interaction.h()->evaluate(xij);
                        }

                        // Compute pair potential and its derivative.
                        double V_AB = interaction.V()->evaluate(rij);
                        totalEnergy += h_AB * V_AB;
                    }
                }
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
 * Computes the total energy and forces of the structure.
 ******************************************************************************/
double CDIPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;
    int inum = data.neighborList.numAtoms();
    int ntypes = job()->numAtomTypes();
    int ninteractions = _pairInteractions.size();
    vector<Vector3>& forces = structure.atomForces();
    BOOST_ASSERT(inum == structure.numLocalAtoms());

    // Allocate data arrays.
    vector<PerAtomAndTypeInfo> perAtomAndTypeArray(inum * ntypes);
    vector<PerAtomAndInteractionInfo> perAtomAndInteractionArray(inum * ninteractions);

    // Reset density and M & N arrays to zero.
    for(vector<PerAtomAndTypeInfo>::iterator i = perAtomAndTypeArray.begin(); i != perAtomAndTypeArray.end(); ++i)
        memset(&*i, 0, sizeof(PerAtomAndTypeInfo));
    for(vector<PerAtomAndInteractionInfo>::iterator i = perAtomAndInteractionArray.begin(); i != perAtomAndInteractionArray.end();
        ++i)
        memset(&*i, 0, sizeof(PerAtomAndInteractionInfo));

    // Calculate sigma_bar and sigma_bar_S as well as their gradients.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        BOOST_ASSERT(i < structure.numLocalAtoms());
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij >= _cutoff) continue;

            int j = neighbor_j->index;
            int jlocal = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            BOOST_ASSERT(i * ntypes < perAtomAndTypeArray.size());
            BOOST_ASSERT(jlocal * ntypes < perAtomAndTypeArray.size());
            vector<PerAtomAndTypeInfo>::iterator atomInfo_i = perAtomAndTypeArray.begin() + (i * ntypes);
            vector<PerAtomAndTypeInfo>::iterator atomInfo_j = perAtomAndTypeArray.begin() + (jlocal * ntypes);
            for(int t = 0; t < ntypes; t++, ++atomInfo_i, ++atomInfo_j) {
                BOOST_ASSERT(t < _sigmas.size());
                BOOST_ASSERT(_sigmas[t] != NULL);
                double s_prime;
                double s = _sigmas[t]->evaluate(rij, s_prime);
                atomInfo_i->sigma_bar += s;
                if(!_legacyMode) {
                    s_prime /= rij;
                    Vector3 d = s_prime * neighbor_j->delta;
                    atomInfo_i->sigma_bar_grad -= d;
                    if(jtype == t) {
                        atomInfo_i->sigma_bar_S += s;
                        atomInfo_i->sigma_bar_S_grad -= d;
                    }
                    atomInfo_j->sigma_bar += s;
                    atomInfo_j->sigma_bar_grad += d;
                    if(itype == t) {
                        atomInfo_j->sigma_bar_S += s;
                        atomInfo_j->sigma_bar_S_grad += d;
                    }
                }
                else {
                    if(jtype == t) atomInfo_i->sigma_bar_S += s;
                    atomInfo_j->sigma_bar += s;
                    if(itype == t) atomInfo_j->sigma_bar_S += s;
                }
            }
        }
    }

    // Compute M and N for each atom.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij >= _cutoff) continue;

            int j = neighbor_j->index;
            int jlocal = neighbor_j->localIndex;
            int jtype = structure.atomType(j);
            BOOST_ASSERT(i * ninteractions < perAtomAndInteractionArray.size());
            BOOST_ASSERT(jlocal * ninteractions < perAtomAndInteractionArray.size());
            vector<PerAtomAndInteractionInfo>::iterator atomInfo_i = perAtomAndInteractionArray.begin() + (i * ninteractions);
            vector<PerAtomAndInteractionInfo>::iterator atomInfo_j =
                perAtomAndInteractionArray.begin() + (jlocal * ninteractions);
            for(int pi = 0; pi < ninteractions; pi++, ++atomInfo_i, ++atomInfo_j) {
                const CDIPairInteraction& interaction = *_pairInteractions[pi];
                if(interaction.interacting(itype, jtype)) {
                    // Compute pair potential.
                    double V_AB = interaction.V()->evaluate(rij);

                    BOOST_ASSERT(interaction.matrix() >= 0 && interaction.matrix() < _sigmas.size());
                    if(!_legacyMode) {
                        // Compute x_i(j)^S
                        double sigma_ij = _sigmas[interaction.matrix()]->evaluate(rij);
                        double sigma_bar_ij_S = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar_S;
                        if(jtype == interaction.matrix()) sigma_bar_ij_S -= sigma_ij;
                        double sigma_bar_ij = (perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar - sigma_ij);
                        double x_ij_S = sigma_bar_ij_S / sigma_bar_ij;

                        // Compute x_j(i)^S
                        double sigma_bar_ji_S = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar_S;
                        if(itype == interaction.matrix()) sigma_bar_ji_S -= sigma_ij;
                        double sigma_bar_ji = (perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar - sigma_ij);
                        double x_ji_S = sigma_bar_ji_S / sigma_bar_ji;

                        if(_linearizedMode) {
                            // Evaluate h'(x_i(j))
                            double h_AB_prime_ij;
                            interaction.h()->evaluate(x_ij_S, h_AB_prime_ij);

                            double Mi_inc = V_AB * h_AB_prime_ij / sigma_bar_ij;
                            atomInfo_i->M += Mi_inc;
                            atomInfo_i->N += Mi_inc * x_ij_S;

                            // Evaluate h'(x_j(i))
                            double h_AB_prime_ji;
                            interaction.h()->evaluate(x_ji_S, h_AB_prime_ji);

                            double Mj_inc = V_AB * h_AB_prime_ji / sigma_bar_ji;
                            atomInfo_j->M += Mj_inc;
                            atomInfo_j->N += Mj_inc * x_ji_S;
                        }
                        else {
                            double xij = 0.5 * (x_ij_S + x_ji_S);

                            // Evaluate h'(xij).
                            double h_AB_prime;
                            interaction.h()->evaluate(xij, h_AB_prime);

                            double Mi_inc = V_AB * h_AB_prime / sigma_bar_ij;
                            atomInfo_i->M += Mi_inc;
                            atomInfo_i->N += Mi_inc * x_ij_S;
                            double Mj_inc = V_AB * h_AB_prime / sigma_bar_ji;
                            atomInfo_j->M += Mj_inc;
                            atomInfo_j->N += Mj_inc * x_ji_S;
                        }
                    }
                    else {
                        if(_linearizedMode) {
                            atomInfo_i->M += V_AB;
                            atomInfo_j->M += V_AB;
                        }
                        else {
                            // Compute x_i^S
                            double sigma_bar_i_S = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar_S;
                            double sigma_bar_i = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar;
                            double x_i = sigma_bar_i_S / sigma_bar_i;

                            // Compute x_j^S
                            double sigma_bar_j_S = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar_S;
                            double sigma_bar_j = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar;
                            double x_j = sigma_bar_j_S / sigma_bar_j;

                            double xij = 0.5 * (x_i + x_j);

                            // Evaluate h'(xij).
                            double h_AB_prime;
                            interaction.h()->evaluate(xij, h_AB_prime);

                            atomInfo_i->M += V_AB * h_AB_prime / sigma_bar_i;
                            atomInfo_j->M += V_AB * h_AB_prime / sigma_bar_j;
                        }
                    }
                }
            }
        }
    }

    if(_legacyMode && _linearizedMode) {
        // Complete computation of M_i values by multiplying with prefactor (independent of j).
        vector<PerAtomAndInteractionInfo>::iterator atomInfo_i = perAtomAndInteractionArray.begin();
        for(int i = 0; i < inum; i++) {
            for(int pi = 0; pi < ninteractions; pi++, ++atomInfo_i) {
                const CDIPairInteraction& interaction = *_pairInteractions[pi];

                double sigma_bar_i_S = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar_S;
                double sigma_bar_i = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar;
                double x_i = sigma_bar_i_S / sigma_bar_i;

                double h_AB_prime;
                interaction.h()->evaluate(x_i, h_AB_prime);

                atomInfo_i->M *= h_AB_prime / sigma_bar_i;
            }
        }
        BOOST_ASSERT(atomInfo_i == perAtomAndInteractionArray.end());
    }

    // Finally, compute pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij >= _cutoff) continue;

            int j = neighbor_j->index;
            int jlocal = neighbor_j->localIndex;
            int jtype = structure.atomType(j);

            BOOST_ASSERT(jtype == structure.atomType(jlocal));
            BOOST_ASSERT(i * ninteractions < perAtomAndInteractionArray.size());
            BOOST_ASSERT(jlocal * ninteractions < perAtomAndInteractionArray.size());
            vector<PerAtomAndInteractionInfo>::iterator atomInfo_i = perAtomAndInteractionArray.begin() + (i * ninteractions);
            vector<PerAtomAndInteractionInfo>::iterator atomInfo_j =
                perAtomAndInteractionArray.begin() + (jlocal * ninteractions);
            for(int pi = 0; pi < ninteractions; pi++, ++atomInfo_i, ++atomInfo_j) {
                const CDIPairInteraction& interaction = *_pairInteractions[pi];

                double sigma_ij_prime;
                double sigma_ij = _sigmas[interaction.matrix()]->evaluate(rij, sigma_ij_prime);

                if(!_legacyMode) {
                    // Compute x_i(j)^S
                    const PerAtomAndTypeInfo& atomTypeInfo_i = perAtomAndTypeArray[i * ntypes + interaction.matrix()];
                    double sigma_bar_ij_S = atomTypeInfo_i.sigma_bar_S;
                    if(jtype == interaction.matrix()) sigma_bar_ij_S -= sigma_ij;
                    double sigma_bar_ij = (atomTypeInfo_i.sigma_bar - sigma_ij);
                    double x_ij_S = sigma_bar_ij_S / sigma_bar_ij;

                    // Compute x_j(i)^S
                    const PerAtomAndTypeInfo& atomTypeInfo_j = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()];
                    double sigma_bar_ji_S = atomTypeInfo_j.sigma_bar_S;
                    if(itype == interaction.matrix()) sigma_bar_ji_S -= sigma_ij;
                    double sigma_bar_ji = (atomTypeInfo_j.sigma_bar - sigma_ij);
                    double x_ji_S = sigma_bar_ji_S / sigma_bar_ji;

                    double V_AB_prime;
                    double V_AB = interaction.V()->evaluate(rij, V_AB_prime);

                    double h_AB;
                    double f_i = 0;
                    double f_j = 0;
                    if(_linearizedMode) {
                        // Evaluate h(x_i(j))
                        double h_ij_prime;
                        double h_ij = interaction.h()->evaluate(x_ij_S, h_ij_prime);
                        // Evaluate h(x_j(i))
                        double h_ji_prime;
                        double h_ji = interaction.h()->evaluate(x_ji_S, h_ji_prime);
                        // Compute final h by linear interpolation.
                        h_AB = 0.5 * (h_ij + h_ji);

                        if(interaction.interacting(itype, jtype)) {
                            double prefactor = V_AB;
                            f_i += prefactor * (h_ji_prime * x_ji_S / sigma_bar_ji + h_ij_prime * x_ij_S / sigma_bar_ij);
                            f_j += prefactor * (h_ij_prime * x_ij_S / sigma_bar_ij + h_ji_prime * x_ji_S / sigma_bar_ji);
                            if(itype == interaction.matrix()) {
                                f_i -= prefactor * h_ji_prime / sigma_bar_ji;
                                f_j -= prefactor * h_ji_prime / sigma_bar_ji;
                            }
                            if(jtype == interaction.matrix()) {
                                f_i -= prefactor * h_ij_prime / sigma_bar_ij;
                                f_j -= prefactor * h_ij_prime / sigma_bar_ij;
                            }
                            forces[i] -= 0.5 * prefactor * h_ij_prime / sigma_bar_ij *
                                         (atomTypeInfo_i.sigma_bar_S_grad - atomTypeInfo_i.sigma_bar_grad * x_ij_S);
                            forces[jlocal] -= 0.5 * prefactor * h_ji_prime / sigma_bar_ji *
                                              (atomTypeInfo_j.sigma_bar_S_grad - atomTypeInfo_j.sigma_bar_grad * x_ji_S);
                        }
                    }
                    else {
                        double xij = 0.5 * (x_ij_S + x_ji_S);
                        // Evaluate h(xij).
                        double h_AB_prime;
                        h_AB = interaction.h()->evaluate(xij, h_AB_prime);

                        if(interaction.interacting(itype, jtype)) {
                            double prefactor = h_AB_prime * V_AB;
                            f_i += prefactor * (x_ji_S / sigma_bar_ji + x_ij_S / sigma_bar_ij);
                            f_j += prefactor * (x_ij_S / sigma_bar_ij + x_ji_S / sigma_bar_ji);
                            if(itype == interaction.matrix()) {
                                f_i -= prefactor / sigma_bar_ji;
                                f_j -= prefactor / sigma_bar_ji;
                            }
                            if(jtype == interaction.matrix()) {
                                f_i -= prefactor / sigma_bar_ij;
                                f_j -= prefactor / sigma_bar_ij;
                            }
                            forces[i] -= 0.5 * prefactor / sigma_bar_ij *
                                         (atomTypeInfo_i.sigma_bar_S_grad - atomTypeInfo_i.sigma_bar_grad * x_ij_S);
                            forces[jlocal] -= 0.5 * prefactor / sigma_bar_ji *
                                              (atomTypeInfo_j.sigma_bar_S_grad - atomTypeInfo_j.sigma_bar_grad * x_ji_S);
                        }
                    }

                    f_i -= atomInfo_j->N;
                    if(itype == interaction.matrix()) f_i += atomInfo_j->M;
                    f_i *= 0.5 * sigma_ij_prime;

                    f_j -= atomInfo_i->N;
                    if(jtype == interaction.matrix()) f_j += atomInfo_i->M;
                    f_j *= 0.5 * sigma_ij_prime;

                    if(interaction.interacting(itype, jtype)) {
                        f_i += h_AB * V_AB_prime;
                        f_j += h_AB * V_AB_prime;
                        totalEnergy += h_AB * V_AB;
                    }

                    forces[i] += neighbor_j->delta * (f_i / rij);
                    forces[jlocal] -= neighbor_j->delta * (f_j / rij);
                }
                else {
                    double fpair = 0;
                    double h_AB;

                    // Compute x_i^S
                    double sigma_bar_i_S = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar_S;
                    double sigma_bar_i = perAtomAndTypeArray[i * ntypes + interaction.matrix()].sigma_bar;
                    double x_i = sigma_bar_i_S / sigma_bar_i;

                    // Compute x_j^S
                    double sigma_bar_j_S = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar_S;
                    double sigma_bar_j = perAtomAndTypeArray[jlocal * ntypes + interaction.matrix()].sigma_bar;
                    double x_j = sigma_bar_j_S / sigma_bar_j;

                    // Evaluate h(xij).
                    if(_linearizedMode) {
                        // Evaluate h(x_i)
                        double h_i = interaction.h()->evaluate(x_i);
                        // Evaluate h(x_j)
                        double h_j = interaction.h()->evaluate(x_j);
                        // Compute final h by linear interpolation.
                        h_AB = 0.5 * (h_i + h_j);
                    }
                    else {
                        double xij = 0.5 * (x_i + x_j);
                        h_AB = interaction.h()->evaluate(xij);
                    }

                    if(interaction.interacting(itype, jtype)) {
                        // Compute pair potential and its derivative.
                        double V_AB_prime;
                        double V_AB = interaction.V()->evaluate(rij, V_AB_prime);
                        fpair += h_AB * V_AB_prime;
                        totalEnergy += h_AB * V_AB;
                    }

                    double M_i_factor;
                    double M_j_factor;
                    if(jtype == interaction.matrix())
                        M_i_factor = 1 - x_i;
                    else
                        M_i_factor = -x_i;
                    if(itype == interaction.matrix())
                        M_j_factor = 1 - x_j;
                    else
                        M_j_factor = -x_j;

                    fpair += 0.5 * sigma_ij_prime * (atomInfo_j->M * M_j_factor + atomInfo_i->M * M_i_factor);

                    Vector3 fvec = neighbor_j->delta * (fpair / rij);
                    forces[i] += fvec;
                    forces[jlocal] -= fvec;
                }
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
 * This function is called by the fit job on shutdown, i.e. after the fitting
 * process has finished.
 ******************************************************************************/
void CDIPotential::outputResults()
{
    Potential::outputResults();

    if(_exportPotentialFile.empty() == false) {
        MsgLogger(medium) << "Writing CDIP file to " << makePathRelative(_exportPotentialFile) << endl;
        writePotential(_exportPotentialFile);
    }

    if(_exportFunctionsFile.empty() == false) {
        MsgLogger(medium) << "Writing CDIP function tables to " << makePathRelative(_exportFunctionsFile) << endl;
        writeTables(_exportFunctionsFile);
    }
}

/******************************************************************************
 * Generates a potential file to be used with simulation codes.
 ******************************************************************************/
void CDIPotential::writePotential(const FPString& filename) const
{
    ofstream out(filename.c_str());
    if(!out.is_open()) throw runtime_error(str(format("Could not open potential file for writing: %1%") % filename));

    out << setprecision(12);

    // The fit job may contain more atom types than what we write to the potential file.
    // Thus, atom types need to be renumbered.
    int ntypes = 0;
    vector<int> typeMapping(job()->numAtomTypes());
    for(int atype = 0; atype < job()->numAtomTypes(); atype++) {
        if(_sigmas[atype] != NULL) typeMapping[atype] = ++ntypes;
    }

    // Write format version line.
    out << "CDIP 1.0" << endl;

    // Write comment line.
    out << job()->name() << " (" << boost::gregorian::day_clock::local_day() << ")" << endl;

    // Write flags.
    out << "Linearized mode" << endl << _linearizedMode << endl;
    out << "Legacy concentration mode" << endl << _legacyMode << endl;

    // Sigma functions.
    out << "Sigma functions" << endl;
    int nr = 200;
    double dr = cutoff() / (nr - 1);
    out << ntypes << " " << cutoff() << " " << nr << " " << dr << endl;
    double left_deriv, right_deriv;
    for(int atype = 0; atype < job()->numAtomTypes(); atype++) {
        std::shared_ptr<FunctionBase> sigmaFunc = _sigmas[atype];
        if(!sigmaFunc) continue;
        sigmaFunc->evaluate(0, left_deriv);
        sigmaFunc->evaluate(cutoff(), right_deriv);
        out << typeMapping[atype] << " " << job()->atomTypeName(atype) << endl;
        out << left_deriv << " " << right_deriv << endl;
        for(int i = 0; i < nr; i++) out << sigmaFunc->evaluate(dr * i) << " ";
        out << endl;
    }

    // Pair potentials.
    out << "Pair potentials" << endl;
    out << _pairInteractions.size() << " " << cutoff() << " " << nr << " " << dr << endl;
    for(const std::shared_ptr<CDIPairInteraction>& interaction : _pairInteractions) {
        out << typeMapping[interaction->atomTypeA()] << " " << typeMapping[interaction->atomTypeB()] << " "
            << typeMapping[interaction->matrix()] << endl;
        interaction->V()->evaluate(0, left_deriv);
        interaction->V()->evaluate(cutoff(), right_deriv);
        out << left_deriv << " " << right_deriv << endl;
        for(int i = 0; i < nr; i++) out << interaction->V()->evaluate(dr * i) << " ";
        out << endl;
    }

    // Pair interpolation functions.
    out << "Pair interpolation functions" << endl;
    int nx = 200;
    double dx = 1.0 / (nx - 1);
    out << _pairInteractions.size() << " " << 1.0 << " " << nx << " " << dx << endl;
    for(const std::shared_ptr<CDIPairInteraction>& interaction : _pairInteractions) {
        out << typeMapping[interaction->atomTypeA()] << " " << typeMapping[interaction->atomTypeB()] << " "
            << typeMapping[interaction->matrix()] << endl;
        interaction->h()->evaluate(0, left_deriv);
        interaction->h()->evaluate(1, right_deriv);
        out << left_deriv << " " << right_deriv << endl;
        for(int i = 0; i < nx; i++) out << interaction->h()->evaluate(dx * i) << " ";
        out << endl;
    }
}

/******************************************************************************
 * Write potential tables to files for visualization with Gnuplot.
 ******************************************************************************/
void CDIPotential::writeTables(const FPString& basename) const
{
    double samplingResolution = 200.0;

    // Write concentration function tables.
    double rmin = 0.2, rmax = cutoff();
    double dr = (rmax - rmin) / samplingResolution;
    for(int atype = 0; atype < job()->numAtomTypes(); atype++) {
        std::shared_ptr<FunctionBase> sigma = _sigmas[atype];
        if(!sigma) continue;
        FPString filename = str(format("%1%.sigma.%2%.table") % basename % job()->atomTypeName(atype));
        MsgLogger(maximum) << "  " << makePathRelative(filename) << endl;
        ofstream out(filename.c_str());
        if(!out.is_open()) throw runtime_error(str(format("Could not open file for writing: %1%") % filename));
        sigma->writeTabulated(out, rmin, rmax, dr);
    }

    // Write pair potentials.
    rmin = 0.2;
    rmax = cutoff();
    dr = (rmax - rmin) / samplingResolution;
    for(const std::shared_ptr<CDIPairInteraction>& interaction : _pairInteractions) {
        FPString filename =
            str(boost::format("%1%.pair.%2%-%3%_in_%4%.table") % basename % job()->atomTypeName(interaction->atomTypeA()) %
                job()->atomTypeName(interaction->atomTypeB()) % job()->atomTypeName(interaction->matrix()));
        MsgLogger(maximum) << "  " << makePathRelative(filename) << endl;
        ofstream out(filename.c_str());
        if(!out.is_open()) throw runtime_error(str(format("Could not open file for writing: %1%") % filename));
        interaction->V()->writeTabulated(out, rmin, rmax, dr);
    }

    // Write interpolation functions.
    rmin = 0;
    rmax = 1;
    dr = (rmax - rmin) / samplingResolution;
    for(const std::shared_ptr<CDIPairInteraction>& interaction : _pairInteractions) {
        FPString filename =
            str(boost::format("%1%.interpol.%2%-%3%_in_%4%.table") % basename % job()->atomTypeName(interaction->atomTypeA()) %
                job()->atomTypeName(interaction->atomTypeB()) % job()->atomTypeName(interaction->matrix()));
        MsgLogger(maximum) << "  " << makePathRelative(filename) << endl;
        ofstream out(filename.c_str());
        if(!out.is_open()) throw runtime_error(str(format("Could not open file for writing: %1%") % filename));
        interaction->h()->writeTabulated(out, rmin, rmax, dr);
    }
}

/******************************************************************************
 * Parses any potential-specific parameters in the XML element in the job file.
 ******************************************************************************/
void CDIPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    // First parse sigma functions block.
    parseSigmas(potentialElement.expectChildElement("sigmas"));

    // Then parse interactions block.
    parseInteractions(potentialElement.expectChildElement("interactions"));

    // Parse mode options.
    _linearizedMode = potentialElement.parseOptionalBooleanParameterAttribute("linearized", false);
    _legacyMode = potentialElement.parseOptionalBooleanParameterAttribute("legacy", false);

    // Determine maximum cutoff.
    _cutoff = 0.0;
    for(const std::shared_ptr<FunctionBase>& func : _sigmas) _cutoff = max(func->cutoff(), _cutoff);
    for(const std::shared_ptr<CDIPairInteraction>& interaction : _pairInteractions)
        _cutoff = max(interaction->V()->cutoff(), _cutoff);

    // Parse output options.
    _exportPotentialFile = potentialElement.parseOptionalPathParameterElement("export-potential");
    _exportFunctionsFile = potentialElement.parseOptionalPathParameterElement("export-functions");

    MsgLogger(maximum) << "CDIP potential '" << id() << "':" << endl;
    MsgLogger(maximum) << "  CDIP linearized mode : " << _linearizedMode << endl;
    MsgLogger(maximum) << "  CDIP legacy mode     : " << _legacyMode << endl;
    MsgLogger(maximum) << "  Maximum cutoff       : " << _cutoff << endl;

#if 0
	// Validate linear functions numerically:
	Q_FOREACH(const QSharedPointer<FunctionBase>& func, _sigmas)
		if(func) func->checkDerivativeNumerically(func->cutoff() / 2);
	Q_FOREACH(const QSharedPointer<CDIPairInteraction>& func, _pairInteractions) {
		if(func) func->V()->checkDerivativeNumerically(func->V()->cutoff() / 2);
		if(func) func->h()->checkDerivativeNumerically(func->h()->cutoff() / 2);
	}
#endif
}

/******************************************************************************
 * Parse concentration function definitions in the XML element in the job file.
 ******************************************************************************/
void CDIPotential::parseSigmas(XML::Element parentElement)
{
    // Allocate empty array of sigma functions, one for each atom type.
    _sigmas.resize(job()->numAtomTypes());

    // Iterate over all function elements.
    for(XML::Element sigElement = parentElement.firstChildElement(); sigElement; sigElement = sigElement.nextSibling()) {
        // Get atom type(s) for which this function is to be applied.
        vector<int> atomTypes = job()->parseAtomTypesAttribute(sigElement, "species");

        // Parse function object.
        std::shared_ptr<FunctionBase> func =
            FunctionBase::createAndParse(sigElement, str(format("sigma%1%") % job()->atomTypeName(atomTypes.front())), job());
        if(!func->hasCutoff())
            throw runtime_error(
                str(format("CDIP sigma function '%1%' (%2%) in line %3% of XML file doesn't have a cutoff radius.") % func->id() %
                    func->tag() % sigElement.lineNumber()));

        // Normalize sigma functions.
        func->setNormalizationMode(FunctionBase::SPHERICAL_INTEGRAL);

        // Register sigma function as a sub-object to this fit object to publish its DOFs.
        registerSubObject(func.get());

        // Check function.
        if(func->evaluate(func->cutoff()) != 0)
            throw runtime_error(
                str(format("CDIP sigma function '%1%' (%2%) in line %3% of XML file is not zero at cutoff radius.") % func->id() %
                    func->tag() % sigElement.lineNumber()));

        for(int atomType : atomTypes) {
            // Consistency check on atom type.
            if(!isAtomTypeEnabled(atomType))
                throw runtime_error(str(format("Species %1% of sigma function not allowed in line %2% of XML file.") %
                                        job()->atomTypeName(atomType) % sigElement.lineNumber()));

            // Assign function object.
            _sigmas[atomType] = func;
        }
    }
}

/******************************************************************************
 * Parse pair interaction definitions in the XML element in the job file.
 ******************************************************************************/
void CDIPotential::parseInteractions(XML::Element parentElement)
{
    // Iterate over all sub-elements in the XML file.
    for(XML::Element interactionElement = parentElement.firstChildElement(); interactionElement;
        interactionElement = interactionElement.nextSibling()) {
        interactionElement.expectTag("pair-interaction");

        // Get atom types to which this interaction applies.
        int atomTypeA = job()->parseAtomTypeAttribute(interactionElement, "species-a");
        int atomTypeB = job()->parseAtomTypeAttribute(interactionElement, "species-b");

        // Get third atom species that controls the interaction strength.
        int atomTypeMatrix = job()->parseAtomTypeAttribute(interactionElement, "matrix");
        if(!_sigmas[atomTypeMatrix])
            throw runtime_error(str(format("Error in CDIP interpolation function definition in line %1% of XML file: Matrix atom "
                                           "type has no sigma function.") %
                                    interactionElement.lineNumber()));

        // Consistency check on atom types.
        if(!isAtomTypeEnabled(atomTypeA))
            throw runtime_error(str(format("Invalid atom type A in line %1% of XML file.") % interactionElement.lineNumber()));
        if(!isAtomTypeEnabled(atomTypeB))
            throw runtime_error(str(format("Invalid atom type B in line %1% of XML file.") % interactionElement.lineNumber()));
        if(!isAtomTypeEnabled(atomTypeMatrix) || _sigmas[atomTypeMatrix] == NULL)
            throw runtime_error(str(format("Invalid matrix type in line %1% of XML file.") % interactionElement.lineNumber()));

        // Parse pair-wise potential function.
        XML::Element potentialElement = interactionElement.expectChildElement("pair-potential");
        XML::Element potfuncElement = potentialElement.firstChildElement();
        if(!potfuncElement)
            throw runtime_error(str(format("Element in line %1% of XML file doesn't contain a function definition.") %
                                    potentialElement.lineNumber()));
        std::shared_ptr<FunctionBase> V = FunctionBase::createAndParse(potfuncElement, "pair", job());
        if(!V->hasCutoff())
            throw runtime_error(str(format("CDIP potential function in line %1% of XML file doesn't have a cutoff radius.") %
                                    potfuncElement.lineNumber()));

        // Check function.
        if(V->evaluate(V->cutoff()) != 0)
            throw runtime_error(str(format("CDIP potential function in line %1% of XML file is not zero at cutoff radius.") %
                                    potfuncElement.lineNumber()));

        // Parse interpolation function.
        XML::Element interpolationElement = interactionElement.expectChildElement("interpolation");
        XML::Element interpolfuncElement = interpolationElement.firstChildElement();
        if(!interpolfuncElement)
            throw runtime_error(str(format("Element in line %1% of XML file doesn't contain a function definition.") %
                                    interpolationElement.lineNumber()));
        std::shared_ptr<FunctionBase> h = FunctionBase::createAndParse(interpolfuncElement, "interpol", job());

        // Check cutoff of interpolation function.
        if(!h->hasCutoff() || h->cutoff() > 1)
            throw runtime_error(str(format("CDIP interpolation function in line %1% has an invalid cutoff.") %
                                    interpolationElement.lineNumber()));
        if(h->evaluate(1.0) != 0)
            MsgLogger(medium) << "Warning: CDIP interpolation function in line " << interpolationElement.lineNumber()
                              << " is not zero at x = 1." << endl;

        // Normalize interpolation function to unity at x=0.
        h->setNormalizationMode(FunctionBase::UNITY_ORIGIN);

        // Create interaction object.
        FPString interactionId = str(format("%1%-%2%_%3%") % job()->atomTypeName(atomTypeA) % job()->atomTypeName(atomTypeB) %
                                     job()->atomTypeName(atomTypeMatrix));
        std::shared_ptr<CDIPairInteraction> interaction(
            new CDIPairInteraction(interactionId, job(), atomTypeA, atomTypeB, atomTypeMatrix, V, h));

        // Register interaction.
        registerSubObject(interaction.get());
        _pairInteractions.push_back(interaction);
    }
}

/******************************************************************************
 * Produces an XML representation of the potential's current parameter values
 * and DOFs that can be used as input in a subsequent fit job.
 ******************************************************************************/
XML::OElement CDIPotential::generateXMLDefinition()
{
    XML::OElement root("cdip");

    // Output mode options.
    if(_linearizedMode)
        root.setAttribute("linearized", "true");
    else
        root.setAttribute("linearized", "false");

    if(_legacyMode)
        root.setAttribute("legacy", "true");
    else
        root.setAttribute("legacy", "false");

    XML::OElement sigmas("sigmas");
    root.appendChild(sigmas);

    for(int atype = 0; atype < job()->numAtomTypes(); atype++) {
        std::shared_ptr<FunctionBase> sigma = _sigmas[atype];
        if(!sigma) continue;
        XML::OElement sigmaElement = sigma->generateXMLDefinition(sigma->tag());
        sigmas.appendChild(sigmaElement);
        sigmaElement.setAttribute("species", job()->atomTypeName(atype));
    }

    XML::OElement interactions("interactions");
    root.appendChild(interactions);

    for(const std::shared_ptr<CDIPairInteraction>& interaction : _pairInteractions) {
        XML::OElement interactionElement("pair-interaction");
        interactions.appendChild(interactionElement);
        interactionElement.setAttribute("species-a", job()->atomTypeName(interaction->atomTypeA()));
        interactionElement.setAttribute("species-b", job()->atomTypeName(interaction->atomTypeB()));
        interactionElement.setAttribute("matrix", job()->atomTypeName(interaction->matrix()));

        XML::OElement potentialElement("pair-potential");
        interactionElement.appendChild(potentialElement);

        XML::OElement potentialFuncElement = interaction->V()->generateXMLDefinition(interaction->V()->tag());
        potentialElement.appendChild(potentialFuncElement);

        XML::OElement interpolationElement("interpolation");
        interactionElement.appendChild(interpolationElement);

        XML::OElement interpolationFuncElement =
            interaction->h()->innerFunction()->generateXMLDefinition(interaction->h()->innerFunction()->tag());
        interpolationElement.appendChild(interpolationFuncElement);
    }

    return root;
}
}
