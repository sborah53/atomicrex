///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "../dof/DegreeOfFreedom.h"

namespace atomicrex {

/**
   @brief This class defines the Tersoff potential format.
   @details The Tersoff potential in atomicrex follows the implementation in LAMMPS.
   The potential can be written
   \f[
   E = \frac{1}{2} \sum_i \sum_{j\neq i} V_{ij}
   \f]
   with
   \f[
   V_{ij} = f_C(r_{ij})[f_R(r_{ij}) + b_{ij} f_A(r_{ij})].
   \f]
   Here, \f$V_R(r) = A\exp(-\lambda_1 r)\f$ and \f$V_A(r) = -B\exp(-\lambda_2 r)\f$ are repulsive
   and attractive pair potential branches, respectively, and \f$f_C(r_{ij})\f$ is a cut-off function that is
   unity and decays sinusodially in the interval \f$(R-D,R+D)\f$, after which it vanishes. The three-body
   contributions arise due to the bond-order parameter
   \f[
   b_{ij} = (1+\beta^n\zeta_{ij}^n)^{-\frac{1}{2n}}
   \f]
   where
   \f[
   \zeta_{ij} = \sum_{k\neq i,j} f_C(r_{ij})g(\theta_{ijk})\exp [\lambda_3^m(r_{ij}-r_{ik})^m].
   \f]
   The angular dependence is due to the factor
   \f[
   g(\theta) = \gamma_{ijk} \left( 1+\frac{c^2}{d^2} - \frac{c^2}{d^2 + (h - \cos \theta)^2}\right).
   \f]
   Parameter files are in LAMMPS tersoff format.

   The following code snippet, to be inserted in the @c potentials block,
   illustrates the definition of this potential type in the input file.
   @code
   <tersoff id="Co" species-a="*" species-b="*">
      <param-file>Co.tersoff</param-file>
      <fit-dof>
         <A tag="CoCoCo" enabled="true" />
         <B tag="CoCoCo" enabled="true" />
         <lambda1 tag="CoCoCo" enabled="true" />
         <lambda2 tag="CoCoCo" enabled="true" />
         <gamma tag="CoCoCo" enabled="true"/>
         <lambda3 tag="CoCoCo" enabled="true" />
         <c tag="CoCoCo" enabled="false" />
         <d tag="CoCoCo" enabled="false" />
         <theta0 tag="CoCoCo" enabled="false" />
      </fit-dof>
   </tersoff>
   @endcode
*/
class TersoffPotential : public Potential
{
public:
    /// Constructor.
    TersoffPotential(const FPString& id, FitJob* job, const FPString& tag = FPString("Tersoff"));

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// This function is called by the fit job on shutdown, i.e. after the fitting process has finished.
    virtual void outputResults() override;

    /// Generates a potential file to be used with simulation codes.
    void writePotential(const FPString& filename) const;

public:
    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

protected:
    struct TersoffParamSet {
        ScalarDOF lam1, lam2, lam3;
        ScalarDOF c, d, h;
        ScalarDOF gamma;
        double powerm, powern;
        ScalarDOF beta;
        ScalarDOF biga, bigb;
        double bigd, bigr;
        double cut;
        double c1, c2, c3, c4;
        int powermint;

        TersoffParamSet()
            : lam1("lambda1", 0.0, 0.0),
              lam2("lambda2", 0.0, 0.0),
              lam3("lambda3"),
              c("c", 0.0, 0.0),
              d("d", 0.0, 0.0),
              h("theta0"),
              beta("beta", 0.0, 0.0),
              gamma("gamma", 0.0, 0.0),
              biga("A", 0.0, 0.0),
              bigb("B", 0.0, 0.0)
        {
        }

        double ters_fc(double r) const
        {
            if(r < bigr - bigd) return 1.0;
            if(r > bigr + bigd) return 0.0;
            return 0.5 * (1.0 - sin(M_PI / 2 * (r - bigr) / bigd));
        }
        double ters_fc_d(double r) const
        {
            if(r < bigr - bigd) return 0.0;
            if(r > bigr + bigd) return 0.0;
            return -(M_PI / 4 / bigd) * cos(M_PI / 2 * (r - bigr) / bigd);
        }
        double ters_fa(double r) const
        {
            if(r > bigr + bigd) return 0.0;
            return -bigb * exp(-lam2 * r) * ters_fc(r);
        }
        double ters_fa_d(double r) const
        {
            if(r > bigr + bigd) return 0.0;
            return bigb * exp(-lam2 * r) * (lam2 * ters_fc(r) - ters_fc_d(r));
        }
        double ters_gijk(double costheta) const
        {
            double ters_c = c * c;
            double ters_d = d * d;
            double hcth = h - costheta;
            return gamma * (1.0 + ters_c / ters_d - ters_c / (ters_d + hcth * hcth));
        }
        double ters_gijk_d(const double costheta) const
        {
            double ters_c = c * c;
            double ters_d = d * d;
            double hcth = h - costheta;
            double numerator = -2.0 * ters_c * hcth;
            double denominator = 1.0 / (ters_d + hcth * hcth);
            return gamma * numerator * denominator * denominator;
        }
        double ters_bij(double zeta) const
        {
            double tmp = beta * zeta;
            if(tmp > c1) return 1.0 / sqrt(tmp);
            if(tmp > c2) return (1.0 - pow(tmp, -powern) / (2.0 * powern)) / sqrt(tmp);
            if(tmp < c4) return 1.0;
            if(tmp < c3) return 1.0 - pow(tmp, powern) / (2.0 * powern);
            return pow(1.0 + pow(tmp, powern), -1.0 / (2.0 * powern));
        }
        double ters_bij_d(double zeta) const
        {
            double tmp = beta * zeta;
            if(tmp > c1) return beta * -0.5 * pow(tmp, -1.5);
            if(tmp > c2) return beta * (-0.5 * pow(tmp, -1.5) * (1.0 - 0.5 * (1.0 + 1.0 / (2.0 * powern)) * pow(tmp, -powern)));
            if(tmp < c4) return 0.0;
            if(tmp < c3) return -0.5 * beta * pow(tmp, powern - 1.0);
            double tmp_n = pow(tmp, powern);
            return -0.5 * pow(1.0 + tmp_n, -1.0 - (1.0 / (2.0 * powern))) * tmp_n / zeta;
        }
        void setInitialParams(double lam1, double lam2, double lam3, double c, double d, double h, double gamma, double powerm,
                              double powern, double beta, double biga, double bigb, double bigd, double bigr)
        {
            // Store parameters.
            this->lam1.setInitialValue(lam1);
            this->lam2.setInitialValue(lam2);
            this->lam3.setInitialValue(lam3);
            this->c.setInitialValue(c);
            this->d.setInitialValue(d);
            this->h.setInitialValue(h);
            this->gamma.setInitialValue(gamma);
            this->powern = powern;
            this->powerm = powerm;
            this->beta.setInitialValue(beta);
            this->biga.setInitialValue(biga);
            this->bigb.setInitialValue(bigb);
            this->bigd = bigd;
            this->bigr = bigr;

            // Compute dependent parameters.
            this->powermint = (int)powerm;
            this->cut = bigr + bigd;
            this->c1 = pow(2.0 * powern * 1.0e-16, -1.0 / powern);
            this->c2 = pow(2.0 * powern * 1.0e-8, -1.0 / powern);
            this->c3 = 1.0 / this->c2;
            this->c4 = 1.0 / this->c1;
        }
    };

    /// Returns the parameter set for the i-j-k triplet interaction.
    const TersoffParamSet& getParamSet(int itype, int jtype, int ktype) const
    {
        BOOST_ASSERT(itype >= 0 && itype < _numAtomTypes);
        BOOST_ASSERT(jtype >= 0 && jtype < _numAtomTypes);
        BOOST_ASSERT(ktype >= 0 && ktype < _numAtomTypes);
        return _params[itype * _numAtomTypes * _numAtomTypes + jtype * _numAtomTypes + ktype];
    }

    // The maximum cutoff radius of the potential.
    double _cutoff;

    // Parameter sets for I-J-K interactions.
    std::vector<TersoffParamSet> _params;

    /// The number of atom types.
    int _numAtomTypes;

    /// Parses Tersoff parameters from a LAMMPS potential file.
    void parseTersoffFile(const FPString& filename);

    /// Potential file that is written after fitting.
    FPString _exportPotentialFile;
};

}  // End of namespace
