///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "EAMPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Computes the total energy of the structure.
******************************************************************************/
double EAMPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        if(isAtomTypeEnabled(itype) == false) continue;
        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* rhoij = electronDensity(itype, jtype)) rho_value += rhoij->evaluate(rij);
                if(FunctionBase* V = pairPotential(itype, jtype)) totalEnergy += 0.5 * V->evaluate(rij);
            }
        }
        if(FunctionBase* U = embeddingEnergy(itype)) totalEnergy += U->evaluate(rho_value);
    }
    return totalEnergy;
}

/******************************************************************************
* Computes the total energy and forces of the structure.
******************************************************************************/
double EAMPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    vector<Vector3>& forces = structure.atomForces();
    std::array<double, 6>& virial = structure.virial();

    // Reset per-atom array.
    EAMAtomData* perAtomData = data.perAtomData<EAMAtomData>();
    memset(perAtomData, 0, sizeof(EAMAtomData) * structure.numLocalAtoms());

    // Compute electron densities.
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* rhoij = electronDensity(itype, jtype)) perAtomData[i].rho += rhoij->evaluate(rij);
                if(FunctionBase* rhoji = electronDensity(jtype, itype))
                    perAtomData[neighbor_j->localIndex].rho += rhoji->evaluate(rij);
            }
        }
    }

    // Compute U(rho) and U'(rho).
    for(int i = 0; i < structure.numLocalAtoms(); i++) {
        int itype = structure.atomType(i);
        FunctionBase* U = embeddingEnergy(itype);
        if(isAtomTypeEnabled(itype) && U) totalEnergy += U->evaluate(perAtomData[i].rho, perAtomData[i].Uprime);
    }

    // Compute two-body pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);

                double rho_prime_ij = 0;
                if(FunctionBase* rhoij = electronDensity(itype, jtype)) rhoij->evaluate(rij, rho_prime_ij);

                double rho_prime_ji = 0;
                if(FunctionBase* rhoji = electronDensity(jtype, itype)) rhoji->evaluate(rij, rho_prime_ji);

                double pair_pot = 0;
                double pair_pot_deriv = 0;
                if(FunctionBase* V = pairPotential(itype, jtype)) pair_pot = V->evaluate(rij, pair_pot_deriv);

                double fpair = 0;
                int j = neighbor_j->localIndex;
                if(isAtomTypeEnabled(itype)) {
                    fpair += rho_prime_ij * perAtomData[i].Uprime;
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                if(isAtomTypeEnabled(jtype)) {
                    fpair += rho_prime_ji * perAtomData[j].Uprime;
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                Vector3 fvec = neighbor_j->delta * (fpair / rij);
                forces[i] += fvec;
                forces[j] -= fvec;

                virial[0] -= neighbor_j->delta.x() * fvec.x();
                virial[1] -= neighbor_j->delta.y() * fvec.y();
                virial[2] -= neighbor_j->delta.z() * fvec.z();
                virial[3] -= neighbor_j->delta.y() * fvec.z();
                virial[4] -= neighbor_j->delta.x() * fvec.z();
                virial[5] -= neighbor_j->delta.x() * fvec.y();
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
* Parses any potential-specific parameters in the XML element in the job file.
******************************************************************************/
void EAMPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    // Parse list of functions.
    XML::Element functionsElement = potentialElement.expectChildElement("functions");
    for(XML::Element funcElement = functionsElement.firstChildElement(); funcElement; funcElement = funcElement.nextSibling()) {
        // Parse function object.
        std::shared_ptr<FunctionBase> func = FunctionBase::createAndParse(funcElement, FPString(), job());

        // Register function.
        registerSubObject(func.get());
        _functions.push_back(func);
    }

    _pairPotentialMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);
    _electronDensityMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);
    _embeddingEnergyMap.resize(job()->numAtomTypes(), nullptr);

    // Parse functional mapping.
    XML::Element mappingElement = potentialElement.expectChildElement("mapping");
    for(XML::Element element = mappingElement.firstChildElement(); element; element = element.nextSibling()) {
        if(element.tagEquals("pair-interaction")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("Pair potential function %1% of EAM potential %2% does not have a valid cutoff radius.") %
                        func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _pairPotentialMap[a * job()->numAtomTypes() + b] = func;
                    _pairPotentialMap[b * job()->numAtomTypes() + a] = func;
                }
            }
        }
        else if(element.tagEquals("electron-density")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> element.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("Electron density function %1% of EAM potential %2% does not have a valid cutoff radius.") %
                        func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _electronDensityMap[a * job()->numAtomTypes() + b] = func;
                }
            }
        }
        else if(element.tagEquals("embedding-energy")) {
            vector<int> speciesList = job()->parseAtomTypesAttribute(element, "species");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            for(int a : speciesList) {
                _embeddingEnergyMap[a] = func;
            }
        }
        else
            throw runtime_error(str(format("Unknown EAM functional mapping element <%1%> in line %2% of XML file.") %
                                    element.tag() % element.lineNumber()));
    }

    // Determine maximum cutoff.
    _cutoff = 0;
    for(const auto& f : _pairPotentialMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }
    for(const auto& f : _electronDensityMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }

    // Parse output options.
    _exportFunctionsFile = potentialElement.parseOptionalPathParameterElement("export-functions");
    _exportEAMFile = potentialElement.parseOptionalPathParameterElement("export-eam-file");
    if(!_exportEAMFile.empty()) {
        XML::Element exportElement = potentialElement.firstChildElement("export-eam-file");
        _exportTableResolution = exportElement.parseOptionalIntParameterAttribute("resolution", _exportTableResolution);
        _rhoExportRangeFactor = exportElement.parseOptionalFloatParameterAttribute("rho-range-factor", _rhoExportRangeFactor);
    }

    MsgLogger(maximum) << " EAM potential - maximum cutoff : " << _cutoff << endl;
}

/******************************************************************************
 * This function is called by the fit job on shutdown, i.e. after the fitting
 * process has finished.
 ******************************************************************************/
void EAMPotential::outputResults()
{
    Potential::outputResults();

    if(_exportFunctionsFile.empty() == false) {
        MsgLogger(medium) << "Writing EAM function tables to " << makePathRelative(_exportFunctionsFile) << ".*" << endl;
        writeTables(_exportFunctionsFile);
    }

    if(_exportEAMFile.empty() == false) {
        MsgLogger(medium) << "Writing tabulated 'eam/fs' file " << makePathRelative(_exportEAMFile) << ":" << endl;
        writeEAMFile(_exportEAMFile);
    }
}

/******************************************************************************
 * Writes the tabulated functionals to a set of text files for visualization with Gnuplot.
 ******************************************************************************/
void EAMPotential::writeTables(const FPString& basename) const
{
    double samplingResolution = 200.0;

    double rmin = 0.2, rmax = cutoff();
    double dr = (rmax - rmin) / samplingResolution;
    double rhomin = 0.0, rhomax = 2.5;
    double drho = (rhomax - rhomin) / samplingResolution;

    // Output functionals.
    for(const std::shared_ptr<FunctionBase>& func : _functions) {
        FPString filename = str(format("%1%.%2%.table") % basename % func->id());
        MsgLogger(maximum) << "  " << makePathRelative(filename) << endl;
        ofstream out(filename.c_str());
        if(!out.is_open()) throw runtime_error(str(format("Could not open file for writing: %1%") % filename));

        // Is it a function used as embedding energy?
        if(std::find(_embeddingEnergyMap.begin(), _embeddingEnergyMap.end(), func.get()) != _embeddingEnergyMap.end()) {
            func->writeTabulated(out, rhomin, rhomax, drho);
        }
        else {
            func->writeTabulated(out, rmin, rmax, dr);
        }
    }
}

/******************************************************************************
 * Generates an 'eam/fs' potential file to be used with other simulation codes like LAMMPS.
 ******************************************************************************/
void EAMPotential::writeEAMFile(const FPString& filename) const
{
    ofstream out(filename.c_str());
    if(!out.is_open()) throw runtime_error(str(format("Could not open EAM output file for writing: %1%") % filename));

    out << setprecision(12);
    out << "EAM file written by atomicrex [version " << ATOMICREX_VERSION_STRING << "]" << endl;
    out << "Fit job: " << job()->name() << endl;
    out << "Date: " << boost::gregorian::day_clock::local_day() << endl;

    // Collect list of elements that will be included in the output file.
    std::vector<int> activeSpecies;
    for(int a = 0; a < job()->numAtomTypes(); a++) {
        if(embeddingEnergy(a) != nullptr) {
            activeSpecies.push_back(a);
        }
    }
    if(activeSpecies.empty())
        throw runtime_error(str(
            format("Cannot export EAM potential '%1%' to a file because it does not define any embedding functions.") % id()));

    // Determine the maximum electron density occurring in any of the structures.
    double rhoCutoff = 0;
    for(AtomicStructure* structure : job()->structures()) {
        structure->updateStructure();
        double maxRho = computeMaximumElectronDensity(*structure, structure->perPotentialData(this));
        if(maxRho > rhoCutoff) rhoCutoff = maxRho;
    }
    if(rhoCutoff <= 0)
        throw runtime_error(str(
            format("Cannot export EAM potential '%1%' to a tabulated EAM file. Could not determine maximum electron density.") %
            id()));
    MsgLogger(medium) << "  Maximum electron density in all structures is " << rhoCutoff
                      << "; export range for embedding energy function is set to " << (rhoCutoff * _rhoExportRangeFactor) << endl;
    rhoCutoff *= _rhoExportRangeFactor;

    // Write element list.
    out << activeSpecies.size();
    for(int a : activeSpecies) {
        MsgLogger(medium) << "  Exporting tabulated EAM functions for atom type '" << job()->atomTypeName(a)
                          << "' (mass=" << job()->atomTypeMass(a) << ", atomic number=" << job()->atomTypeAtomicNumber(a) << ")"
                          << endl;
        out << " " << job()->atomTypeName(a);
    }
    out << endl;

    // Number of sampling points.
    int Nrho = _exportTableResolution;
    int Nr = _exportTableResolution;

    double drho = rhoCutoff / (Nrho - 1);
    double dr = cutoff() / (Nr - 1);
    out << Nrho << " " << drho << " " << Nr << " " << dr << " " << cutoff() << endl;

    // Write list of elements with embedding and electron density functions.
    for(int a : activeSpecies) {
        out << job()->atomTypeAtomicNumber(a) << " " << job()->atomTypeMass(a) << " 0.0 NONE" << endl;

        FunctionBase* embeddingFunc = embeddingEnergy(a);
        for(int i = 0; i < Nrho; i++) {
            double rho = drho * i;
            out << (embeddingFunc ? embeddingFunc->evaluate(rho) : 0.0) << "\n";
        }

        for(int b : activeSpecies) {
            FunctionBase* rhoFunc = electronDensity(a, b);
            for(int i = 0; i < Nr; i++) {
                double r = dr * i;
                out << (rhoFunc ? rhoFunc->evaluate(r) : 0.0) << "\n";
            }
        }
    }

    // Write pair potentials list.
    for(size_t i = 0; i < activeSpecies.size(); i++) {
        for(size_t j = 0; j <= i; j++) {
            FunctionBase* phiFunc = pairPotential(activeSpecies[i], activeSpecies[j]);
            for(int n = 0; n < Nr; n++) {
                double r = dr * n;
                out << ((phiFunc ? phiFunc->evaluate(r) : 0.0) * r) << "\n";
            }
        }
    }
}

/******************************************************************************
 * Computes the maximum electron density occurring in a structure.
 * This method is used by the writeEAMFile() function to determine the range of the
 * embedding energy function to be tabulated.
 ******************************************************************************/
double EAMPotential::computeMaximumElectronDensity(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double maxRho = 0;
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);
        if(isAtomTypeEnabled(itype) == false) continue;
        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* rhoij = electronDensity(itype, jtype)) rho_value += rhoij->evaluate(rij);
            }
        }
        if(rho_value > maxRho) maxRho = rho_value;
    }
    return maxRho;
}
}
