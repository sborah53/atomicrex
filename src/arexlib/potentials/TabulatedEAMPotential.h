///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "functions/GridCubicSpline.h"

namespace atomicrex {

/**
 * Embedded Atom Method (EAM) potential with tabulated functionals.
 */
class TabulatedEAMPotential : public Potential
{
public:
    /// Constructor.
    TabulatedEAMPotential(const FPString& id, FitJob* job) : Potential(id, job, "Tabulated-EAM") {}

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Returns the number of bytes the potential needs per atom to store its intermediate
    /// calculation results during energy/force calculation.
    virtual size_t perAtomDataSize() const override { return sizeof(EAMAtomData); }

public:
    /// Parses the tabulated EAM functionals from the given file in "funcfl" format.
    void parseFuncflEAMFile(const FPString& filename);

    /// Parses the tabulated EAM functionals from the given file in "setfl" format.
    void parseSetflEAMFile(const FPString& filename);

    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

private:
    GridCubicSpline z2r;  // z2r(r_ij)
    GridCubicSpline rho;  // rho(r_ij)
    GridCubicSpline U;    // U(rho)

    double _cutoff;  // The local cutoff radius.

    struct EAMAtomData {
        double rho, Uprime;
    };
};

}  // End of namespace
