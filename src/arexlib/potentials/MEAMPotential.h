///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Potential.h"
#include "functions/Functions.h"

namespace atomicrex {

/**
   @brief This class defines general modified embedded atom method (MEAM) potentials.
   @details The total energy for MEAM potentials can be written in the general form
   \f[
     E = \sum_{ij} V(r_{ij}) + \sum_i F(\rho_i)
   \f]
   where
   \f[
   \rho_i= \sum_j \rho(r_{ij})
   + \sum_{jk} f(r_{ij}) f(r_{ik}) g\left(\cos(\theta_{ijk})\right).
   \f]
   In the present implementation it is possible to compose practically
   arbitrary functional forms using a math parser.

   The following code block illustrates the main blocks in the
   potential definition. More complex cases can be found in the @c
   examples directory.

   @code
   <potentials>

     <meam id='my-meam-potential' species-a='*' species-b='*'>

       <export-functions>meam_out</export-functions>

       <functions>
         <user-function id="V">
       ...
     </user-function>
     ...
       </functions>

       <mapping>
         <pair-interaction species-a='*' species-b='*' function='V' />
    <electron-density species-a='*' species-b='*' function='rho' />
    <f-function species-a='*' species-b='*' function='f' />
    <g-function species-a='*' species-b='*' species-c='*' function='g' />
    <embedding-energy species='*' function='U' />
       </mapping>

     </meam>

   </potentials>
   @endcode

 */
class MEAMPotential : public Potential
{
public:
    /// Constructor.
    MEAMPotential(const FPString& id, FitJob* job) : Potential(id, job, "MEAM") {}

    /// Returns the maximum cutoff of the potential.
    virtual double cutoff() const override { return _cutoff; }

    /// Computes the total energy and forces of the structure.
    virtual double computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Computes the total energy of the structure.
    virtual double computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const override;

    /// Parses any potential-specific parameters in the XML element in the job file.
    virtual void parse(XML::Element potentialElement) override;

    /// Returns the number of bytes the potential needs per atom to store its intermediate
    /// calculation results during energy/force calculation.
    virtual size_t perAtomDataSize() const override { return sizeof(double); }

    /// Returns the number of bytes the potential needs per bond to store its intermediate
    /// calculation results during energy/force calculation.
    virtual size_t perBondDataSize() const override { return sizeof(MEAMBondData); }

    /// Returns the pair potential function for the interaction between the given atom types.
    FunctionBase* pairPotential(int speciesA, int speciesB) const
    {
        return _pairPotentialMap[speciesA * job()->numAtomTypes() + speciesB];
    }

    /// Returns the electron density function for the given pair of atom types.
    FunctionBase* electronDensity(int speciesA, int speciesB) const
    {
        return _electronDensityMap[speciesA * job()->numAtomTypes() + speciesB];
    }

    /// Returns the embedding energy function for the given atom type.
    FunctionBase* embeddingEnergy(int species) const { return _embeddingEnergyMap[species]; }

    /// Returns the f-function for the given pair of atom types.
    FunctionBase* ffunction(int speciesA, int speciesB) const
    {
        return _ffunctionMap[speciesA * job()->numAtomTypes() + speciesB];
    }

    /// Returns the f-function for the given triplet of atom types.
    FunctionBase* gfunction(int speciesA, int speciesB, int speciesC) const
    {
        return _gfunctionMap[speciesA * job()->numAtomTypes() * job()->numAtomTypes() + speciesB * job()->numAtomTypes() +
                             speciesC];
    }

    /// Returns the function with the given ID, or nullptr if no such function is defined.
    FunctionBase* findFunctionById(const FPString& id) const
    {
        for(const auto& f : _functions)
            if(f && f->id() == id) return f.get();
        return nullptr;
    }

    /// This function is called by the fit job on shutdown, i.e. after the fitting process has finished.
    virtual void outputResults() override;

    /// Writes the tabulated functionals to a set of text files for visualization with Gnuplot.
    void writeTables(const FPString& basename) const;

private:
    /// The list of analytical functions.
    std::vector<std::shared_ptr<FunctionBase>> _functions;

    /// The mapping of atom types to pair potential functions.
    std::vector<FunctionBase*> _pairPotentialMap;

    /// The mapping of atom types to electron density functions.
    std::vector<FunctionBase*> _electronDensityMap;

    /// The mapping of atom types to embedding energy functions.
    std::vector<FunctionBase*> _embeddingEnergyMap;

    /// The mapping of atom types to f-functions.
    std::vector<FunctionBase*> _ffunctionMap;

    /// The mapping of atom types to g-functions.
    std::vector<FunctionBase*> _gfunctionMap;

    /// The cutoff radius of the potential.
    double _cutoff;

    /// Table file that is written after fitting.
    FPString _exportFunctionsFile;

    // Helper data structure used to store temporary per-bond values during energy/force calculation.
    struct MEAMBondData {
        double f, fprime;
    };
};

}  // End of namespace
