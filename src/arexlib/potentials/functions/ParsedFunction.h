///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Functions.h"

#include <muParser.h>
#include <boost/thread/tss.hpp>

namespace atomicrex {

/************************************************************
 * A scalar function object that evaluates a user-defined math
 * expression.
 *
 * In addition to the scalar input parameter, the math expression
 * can include arbitrary parameters, which are exposed as
 * degrees of freedom by the function class.
 ************************************************************/
class ParsedFunction : public FunctionBase
{
public:
    /// Constructor.
    ParsedFunction(const FPString& id, FitJob* job) : FunctionBase(id, job, "user-function") {}

    /// Parses the XML definition of the object.
    virtual void parse(XML::Element functionElement) override;

    /// This function is called by the DOFs of this object each time their value changes.
    virtual void dofValueChanged(DegreeOfFreedom& dof) override;

protected:
    /// Evaluates the function at a position x.
    double evaluateInternal(double x) override;

    /// Evaluates the function and its first derivative at a position x.
    double evaluateInternal(double x, double& deriv) override;

private:
    /// Every thread needs its own instance of the muParser class.
    /// In a multithreaded environment this is the data structure created for every individual thread.
    struct ThreadData {
        /// Storage for the current value of the input variable.
        double inputValue;
        /// The math expression parser and evaluator.
        mu::Parser parser;
        /// The math expression parser and evaluator for the first derivative.
        mu::Parser derivativeParser;
        /// The generation of the input parameter set that was used to initialize the parsers.
        unsigned int parameterSet = 0;
    };

    /// Initializes the expression parser for the current thread.
    ThreadData* initializeEvaluator();

    /// A monotonically increasing counter the denotes the current generation of input parameter set.
    /// It is increased every time the input parameters (degrees of freedom) or the function expression itself change.
    unsigned int _parameterSet = 1;

    /// The list of parameters, which can be fit.
    std::vector<ScalarDOF> _params;

    /// Math expression for the function value.
    FPString _expression;

    /// Math expression for the function's first derivative.
    FPString _expressionDerivative;

    /// The name of the function input variable, which occurs in the math expression.
    FPString _inputVarName;
};

}  // End of namespace
