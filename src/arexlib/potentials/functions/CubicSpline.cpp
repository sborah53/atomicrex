///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "CubicSpline.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Calculates the second derivatives of the cubic spline.
******************************************************************************/
void CubicSpline::prepareSpline()
{
    BOOST_ASSERT(knotCount() >= 2);
    vector<double> u(knotCount());
    Y2[0] = -0.5;
    u[0] = (3.0 / (X[1] - X[0])) * ((Y[1] - Y[0]) / (X[1] - X[0]) - derivStart());
    for(int i = 1; i <= knotCount() - 2; i++) {
        double sig = (X[i] - X[i - 1]) / (X[i + 1] - X[i - 1]);
        double p = sig * Y2[i - 1] + 2.0;
        Y2[i] = (sig - 1.0) / p;
        u[i] = (Y[i + 1] - Y[i]) / (X[i + 1] - X[i]) - (Y[i] - Y[i - 1]) / (X[i] - X[i - 1]);
        u[i] = (6.0 * u[i] / (X[i + 1] - X[i - 1]) - sig * u[i - 1]) / p;
    }

    double qn = 0.5;
    double un = (3.0 / (X[_N - 1] - X[_N - 2])) * (derivEnd() - (Y[_N - 1] - Y[_N - 2]) / (X[_N - 1] - X[_N - 2]));
    Y2[_N - 1] = (un - qn * u[_N - 2]) / (qn * Y2[_N - 2] + 1.0);
    for(int k = _N - 2; k >= 0; k--) {
        Y2[k] = Y2[k] * Y2[k + 1] + u[k];
    }
}

/******************************************************************************
 * Evaluates the spline function at position x.
******************************************************************************/
double CubicSpline::eval(double x) const
{
    if(x > X.front() && x < X.back()) {
        // Do interval search.
        int klo = 0;
        int khi = _N - 1;
        while(khi - klo > 1) {
            int k = (khi + klo) / 2;
            if(X[k] > x)
                khi = k;
            else
                klo = k;
        }
        // Do spline interpolation.
        double h = X[khi] - X[klo];
        double a = (X[khi] - x) / h;
        double b = (x - X[klo]) / h;
        return a * Y[klo] + b * Y[khi] + ((a * a * a - a) * Y2[klo] + (b * b * b - b) * Y2[khi]) * (h * h) / 6.0;
    }
    else if(x <= X.front()) {  // Left extrapolation.
        return Y.front() + derivStart() * (x - X.front());
    }
    else {  // Right extrapolation.
        return Y.back() + derivEnd() * (x - X.back());
    }
}

/******************************************************************************
 * Evaluates the spline function and its first derivative at position x.
******************************************************************************/
double CubicSpline::eval(double x, double& deriv) const
{
    if(x > X.front() && x < X.back()) {
        // Do interval search.
        int klo = 0;
        int khi = _N - 1;
        while(khi - klo > 1) {
            int k = (khi + klo) / 2;
            if(X[k] > x)
                khi = k;
            else
                klo = k;
        }
        // Do spline interpolation.
        double h = X[khi] - X[klo];
        double a = (X[khi] - x) / h;
        double b = (x - X[klo]) / h;
        deriv = (Y[khi] - Y[klo]) / h + ((3.0 * b * b - 1.0) * Y2[khi] - (3.0 * a * a - 1.0) * Y2[klo]) * h / 6.0;
        return a * Y[klo] + b * Y[khi] + ((a * a * a - a) * Y2[klo] + (b * b * b - b) * Y2[khi]) * (h * h) / 6.0;
    }
    else if(x <= X[0]) {  // Left extrapolation.
        deriv = derivStart();
        return Y.front() + derivStart() * (x - X.front());
    }
    else {  // Right extrapolation.
        deriv = derivEnd();
        return Y.back() + derivEnd() * (x - X.back());
    }
}

/******************************************************************************
 * Create a Gnuplot script that displays the spline function.
******************************************************************************/
void CubicSpline::writeGnuplot(const FPString& filename, const FPString& title) const
{
    ofstream stream(filename.c_str());
    if(!stream.is_open()) throw runtime_error(str(format("Failed to open Gnuplot file %1% for writing.") % filename));

    stream << "#!/usr/bin/env gnuplot" << endl;
    if(title.empty() == false) stream << "set title \"" << title << "\"" << endl;
    double tmin = X.front() - (X.back() - X.front()) * 0.05;
    double tmax = X.back() + (X.back() - X.front()) * 0.05;
    double delta = (tmax - tmin) / (knotCount() * 200);
    stream << "set xrange [" << tmin << ":" << tmax << "]" << endl;
    stream << "plot '-' with lines notitle, '-' with points notitle pt 3 lc 3" << endl;
    for(double x = tmin; x <= tmax + 1e-8; x += delta) {
        double y = eval(x);
        stream << x << " " << y << endl;
    }
    stream << "e" << endl;
    for(int i = 0; i < knotCount(); i++) {
        stream << X[i] << " " << Y[i] << endl;
    }
    stream << "e" << endl;
}
}
