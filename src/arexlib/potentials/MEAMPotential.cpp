///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "MEAMPotential.h"
#include "../structures/AtomicStructure.h"
#include "../structures/NeighborList.h"
#include "../util/xml/XMLUtilities.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Computes the total energy of the structure.
******************************************************************************/
double MEAMPotential::computeEnergy(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    // Compute charge density and then embedding energy for each atom.
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);

        // Skip atoms for which this potential is not enabled.
        if(isAtomTypeEnabled(itype) == false) continue;

        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* f_ij = ffunction(itype, jtype))
                    neighbor_j->bondData<MEAMBondData>().f = f_ij->evaluate(rij);
                else
                    neighbor_j->bondData<MEAMBondData>().f = 0;
                double partial_sum = 0;
                NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
                for(int kk = 0; kk < jj; kk++, neighbor_k++) {
                    double rik = neighbor_k->r;
                    if(rik < _cutoff) {
                        int ktype = structure.atomType(neighbor_k->index);
                        if(FunctionBase* g_ijk = gfunction(itype, jtype, ktype)) {
                            double cos_theta = neighbor_j->delta.dot(neighbor_k->delta) / (rij * rik);
                            partial_sum += neighbor_k->bondData<MEAMBondData>().f * g_ijk->evaluate(cos_theta);
                        }
                    }
                }

                rho_value += neighbor_j->bondData<MEAMBondData>().f * partial_sum;
                if(FunctionBase* rhoij = electronDensity(itype, jtype)) rho_value += rhoij->evaluate(rij);

                // Add pair contribution to total energy.
                if(FunctionBase* V = pairPotential(itype, jtype)) totalEnergy += 0.5 * V->evaluate(rij);
            }
        }
        if(FunctionBase* U = embeddingEnergy(itype)) totalEnergy += U->evaluate(rho_value);
    }

    return totalEnergy;
}

/******************************************************************************
* Computes the total energy and forces of the structure.
******************************************************************************/
double MEAMPotential::computeEnergyAndForces(AtomicStructure& structure, AtomicStructure::PerPotentialData& data) const
{
    double totalEnergy = 0;

    std::vector<Vector3>& forces = structure.atomForces();
    std::array<double, 6>& virial = structure.virial();
    double* Uprime_values = data.perAtomData<double>();

    // Compute charge density and compute embedding energies.
    int inum = data.neighborList.numAtoms();
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);

        // Skip atoms for which this potential is not enabled.
        if(isAtomTypeEnabled(itype) == false) continue;

        double rho_value = 0;
        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsFull(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);
                if(FunctionBase* f_ij = ffunction(itype, jtype))
                    neighbor_j->bondData<MEAMBondData>().f = f_ij->evaluate(rij, neighbor_j->bondData<MEAMBondData>().fprime);
                else
                    neighbor_j->bondData<MEAMBondData>().f = neighbor_j->bondData<MEAMBondData>().fprime = 0;

                double partial_sum = 0;
                NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
                for(int kk = 0; kk < jj; kk++, neighbor_k++) {
                    double rik = neighbor_k->r;
                    if(rik < _cutoff) {
                        int ktype = structure.atomType(neighbor_k->index);
                        if(FunctionBase* g_ijk = gfunction(itype, jtype, ktype)) {
                            double cos_theta = neighbor_j->delta.dot(neighbor_k->delta) / (rij * rik);
                            partial_sum += neighbor_k->bondData<MEAMBondData>().f * g_ijk->evaluate(cos_theta);
                        }
                    }
                }

                rho_value += neighbor_j->bondData<MEAMBondData>().f * partial_sum;
                if(FunctionBase* rhoij = electronDensity(itype, jtype)) rho_value += rhoij->evaluate(rij);
            }
        }

        double Uprime_i;
        double U_i;
        if(FunctionBase* U = embeddingEnergy(itype))
            U_i = U->evaluate(rho_value, Uprime_i);
        else
            U_i = Uprime_i = 0;
        Uprime_values[i] = Uprime_i;

        totalEnergy += U_i;

        Vector3 forces_i = Vector3::Zero();

        // Compute three-body contributions to force.
        neighbor_j = data.neighborList.neighborList(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            BOOST_ASSERT(rij > 0.0);
            if(rij >= _cutoff) continue;

            double f_rij = neighbor_j->bondData<MEAMBondData>().f;
            double f_rij_prime = neighbor_j->bondData<MEAMBondData>().fprime;
            int jtype = structure.atomType(neighbor_j->index);

            Vector3 forces_j = Vector3::Zero();

            NeighborListEntry* neighbor_k = data.neighborList.neighborList(i);
            for(int kk = 0; kk < jj; kk++, neighbor_k++) {
                double rik = neighbor_k->r;
                BOOST_ASSERT(rik > 0.0);
                if(rik >= _cutoff) continue;
                int ktype = structure.atomType(neighbor_k->index);

                double cos_theta = neighbor_j->delta.dot(neighbor_k->delta) / (rij * rik);
                double g_value, g_prime;
                if(FunctionBase* g_ijk = gfunction(itype, jtype, ktype))
                    g_value = g_ijk->evaluate(cos_theta, g_prime);
                else
                    g_value = g_prime = 0;
                double f_rik = neighbor_k->bondData<MEAMBondData>().f;
                double f_rik_prime = neighbor_k->bondData<MEAMBondData>().fprime;

                double fij = -Uprime_i * g_value * f_rik * f_rij_prime;
                double fik = -Uprime_i * g_value * f_rij * f_rik_prime;

                double prefactor = Uprime_i * f_rij * f_rik * g_prime;
                fij += prefactor / rij * cos_theta;
                fik += prefactor / rik * cos_theta;

                Vector3 fj, fk;
                prefactor /= (rij * rik);

                fij /= rij;
                fj.x() = neighbor_j->delta.x() * fij - neighbor_k->delta.x() * prefactor;
                fj.y() = neighbor_j->delta.y() * fij - neighbor_k->delta.y() * prefactor;
                fj.z() = neighbor_j->delta.z() * fij - neighbor_k->delta.z() * prefactor;

                fik /= rik;
                fk.x() = neighbor_k->delta.x() * fik - neighbor_j->delta.x() * prefactor;
                fk.y() = neighbor_k->delta.y() * fik - neighbor_j->delta.y() * prefactor;
                fk.z() = neighbor_k->delta.z() * fik - neighbor_j->delta.z() * prefactor;

                forces_i -= fj + fk;
                forces_j += fj;
                BOOST_ASSERT(neighbor_k->localIndex < forces.size());
                forces[neighbor_k->localIndex] += fk;

                virial[0] += neighbor_j->delta.x() * fj.x();
                virial[1] += neighbor_j->delta.y() * fj.y();
                virial[2] += neighbor_j->delta.z() * fj.z();
                virial[3] += neighbor_j->delta.y() * fj.z();
                virial[4] += neighbor_j->delta.x() * fj.z();
                virial[5] += neighbor_j->delta.x() * fj.y();

                virial[0] += neighbor_k->delta.x() * fk.x();
                virial[1] += neighbor_k->delta.y() * fk.y();
                virial[2] += neighbor_k->delta.z() * fk.z();
                virial[3] += neighbor_k->delta.y() * fk.z();
                virial[4] += neighbor_k->delta.x() * fk.z();
                virial[5] += neighbor_k->delta.x() * fk.y();
            }

            BOOST_ASSERT(neighbor_j->localIndex < forces.size());
            forces[neighbor_j->localIndex] += forces_j;
        }

        BOOST_ASSERT(i < forces.size());
        forces[i] += forces_i;
    }

    // Compute two-body pair interactions.
    for(int ii = 0; ii < inum; ii++) {
        int i = data.neighborList.atomIndex(ii);
        int itype = structure.atomType(i);

        // Skip atoms for which this potential is not enabled.
        if(isAtomTypeEnabled(itype) == false) continue;

        NeighborListEntry* neighbor_j = data.neighborList.neighborList(i);
        int jnum = data.neighborList.numNeighborsHalf(i);
        for(int jj = 0; jj < jnum; jj++, neighbor_j++) {
            double rij = neighbor_j->r;
            if(rij < _cutoff) {
                int jtype = structure.atomType(neighbor_j->index);

                double rho_prime;
                if(FunctionBase* rhoij = electronDensity(itype, jtype))
                    rhoij->evaluate(rij, rho_prime);
                else
                    rho_prime = 0;
                int j = neighbor_j->localIndex;

                double pair_pot_deriv;
                double pair_pot;
                if(FunctionBase* V = pairPotential(itype, jtype))
                    pair_pot = V->evaluate(rij, pair_pot_deriv);
                else
                    pair_pot = pair_pot_deriv = 0;

                double fpair = 0;
                if(isAtomTypeEnabled(structure.atomType(i))) {
                    fpair += rho_prime * Uprime_values[i];
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                if(isAtomTypeEnabled(structure.atomType(j))) {
                    fpair += rho_prime * Uprime_values[j];
                    fpair += 0.5 * pair_pot_deriv;
                    totalEnergy += 0.5 * pair_pot;
                }
                Vector3 fvec = neighbor_j->delta * (fpair / rij);
                BOOST_ASSERT(i < forces.size() && j < forces.size());
                forces[i] += fvec;
                forces[j] -= fvec;

                virial[0] -= neighbor_j->delta.x() * fvec.x();
                virial[1] -= neighbor_j->delta.y() * fvec.y();
                virial[2] -= neighbor_j->delta.z() * fvec.z();
                virial[3] -= neighbor_j->delta.y() * fvec.z();
                virial[4] -= neighbor_j->delta.x() * fvec.z();
                virial[5] -= neighbor_j->delta.x() * fvec.y();
            }
        }
    }

    return totalEnergy;
}

/******************************************************************************
* Parses any potential-specific parameters in the XML element in the job file.
******************************************************************************/
void MEAMPotential::parse(XML::Element potentialElement)
{
    Potential::parse(potentialElement);

    // Parse list of functions.
    XML::Element functionsElement = potentialElement.expectChildElement("functions");
    for(XML::Element funcElement = functionsElement.firstChildElement(); funcElement; funcElement = funcElement.nextSibling()) {
        // Parse function object.
        std::shared_ptr<FunctionBase> func = FunctionBase::createAndParse(funcElement, FPString(), job());

        // Register function.
        registerSubObject(func.get());
        _functions.push_back(func);
    }

    _pairPotentialMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);
    _electronDensityMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);
    _embeddingEnergyMap.resize(job()->numAtomTypes(), nullptr);
    _ffunctionMap.resize(job()->numAtomTypes() * job()->numAtomTypes(), nullptr);
    _gfunctionMap.resize(job()->numAtomTypes() * job()->numAtomTypes() * job()->numAtomTypes(), nullptr);

    // Parse functional mapping.
    XML::Element mappingElement = potentialElement.expectChildElement("mapping");
    for(XML::Element element = mappingElement.firstChildElement(); element; element = element.nextSibling()) {
        if(element.tagEquals("pair-interaction")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("Pair potential function %1% of EAM potential %2% does not have a valid cutoff radius.") %
                        func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _pairPotentialMap[a * job()->numAtomTypes() + b] = func;
                    _pairPotentialMap[b * job()->numAtomTypes() + a] = func;
                }
            }
        }
        else if(element.tagEquals("electron-density")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> element.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(
                    str(format("Electron density function %1% of MEAM potential %2% does not have a valid cutoff radius.") %
                        func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _electronDensityMap[a * job()->numAtomTypes() + b] = func;
                }
            }
        }
        else if(element.tagEquals("embedding-energy")) {
            vector<int> speciesList = job()->parseAtomTypesAttribute(element, "species");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> elements.") %
                                        element.tag() % element.lineNumber()));
            for(int a : speciesList) {
                _embeddingEnergyMap[a] = func;
            }
        }
        else if(element.tagEquals("f-function")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> element.") %
                                        element.tag() % element.lineNumber()));
            if(!func->hasCutoff())
                throw runtime_error(str(format("'f' function %1% of MEAM potential %2% does not have a valid cutoff radius.") %
                                        func->id() % id()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    _ffunctionMap[a * job()->numAtomTypes() + b] = func;
                }
            }
        }
        else if(element.tagEquals("g-function")) {
            vector<int> speciesAList = job()->parseAtomTypesAttribute(element, "species-a");
            vector<int> speciesBList = job()->parseAtomTypesAttribute(element, "species-b");
            vector<int> speciesCList = job()->parseAtomTypesAttribute(element, "species-c");
            FunctionBase* func = findFunctionById(element.parseStringParameterAttribute("function"));
            if(!func)
                throw runtime_error(str(format("Function referenced by element <%1%> in line %2% of XML file has not been "
                                               "defined in <functions> element.") %
                                        element.tag() % element.lineNumber()));
            for(int a : speciesAList) {
                for(int b : speciesBList) {
                    for(int c : speciesCList) {
                        _gfunctionMap[a * job()->numAtomTypes() * job()->numAtomTypes() + b * job()->numAtomTypes() + c] = func;
                    }
                }
            }
        }
        else
            throw runtime_error(str(format("Unknown EAM functional mapping element <%1%> in line %2% of XML file.") %
                                    element.tag() % element.lineNumber()));
    }

    // Determine maximum cutoff.
    _cutoff = 0;
    for(const auto& f : _pairPotentialMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }
    for(const auto& f : _electronDensityMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }
    for(const auto& f : _ffunctionMap) {
        if(f) {
            BOOST_ASSERT(f->hasCutoff());
            _cutoff = std::max(f->cutoff(), _cutoff);
        }
    }

    // Parse output options.
    _exportFunctionsFile = potentialElement.parseOptionalPathParameterElement("export-functions");

    MsgLogger(maximum) << " MEAM potential - maximum cutoff : " << _cutoff << endl;
}

/******************************************************************************
 * This function is called by the fit job on shutdown, i.e. after the fitting
 * process has finished.
 ******************************************************************************/
void MEAMPotential::outputResults()
{
    Potential::outputResults();

    if(_exportFunctionsFile.empty() == false) {
        MsgLogger(medium) << "Writing MEAM function tables to " << makePathRelative(_exportFunctionsFile) << ".*" << endl;
        writeTables(_exportFunctionsFile);
    }
}

/******************************************************************************
 * Writes the tabulated functionals to a set of text files for visualization with Gnuplot.
 ******************************************************************************/
void MEAMPotential::writeTables(const FPString& basename) const
{
    double samplingResolution = 200.0;

    double rmin = 0.2, rmax = cutoff();
    double dr = (rmax - rmin) / samplingResolution;
    double rhomin = 0.0, rhomax = 2.5;
    double drho = (rhomax - rhomin) / samplingResolution;

    // Output functionals.
    for(const std::shared_ptr<FunctionBase>& func : _functions) {
        FPString filename = str(format("%1%.%2%.table") % basename % func->id());
        MsgLogger(maximum) << "  " << makePathRelative(filename) << endl;
        ofstream out(filename.c_str());
        if(!out.is_open()) throw runtime_error(str(format("Could not open file for writing: %1%") % filename));

        // Is it a function used as embedding energy?
        if(std::find(_embeddingEnergyMap.begin(), _embeddingEnergyMap.end(), func.get()) != _embeddingEnergyMap.end()) {
            func->writeTabulated(out, rhomin, rhomax, drho);
        }
        // Is it a g-function?
        else if(std::find(_gfunctionMap.begin(), _gfunctionMap.end(), func.get()) != _gfunctionMap.end()) {
            func->writeTabulated(out, -1.0, 1.0, 2.0 / samplingResolution);
        }
        else {
            func->writeTabulated(out, rmin, rmax, dr);
        }
    }
}
}
