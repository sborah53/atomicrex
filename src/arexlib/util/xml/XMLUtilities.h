///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../../Atomicrex.h"

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xinclude.h>
#include <libxml/xmlschemastypes.h>
#ifndef LIBXML_XINCLUDE_ENABLED
#error "Program requires libxml2 with XInclude support."
#endif
#ifndef LIBXML_SCHEMAS_ENABLED
#error "Program requires libxml2 with XSD Schema support."
#endif

namespace atomicrex {
namespace XML {

/**
 * Global initialization and clean-up of the libxml2 library in RAII fashion.
 */
struct Context {
    Context()
    {
        ::xmlInitParser();
        LIBXML_TEST_VERSION
    }
    ~Context() { ::xmlCleanupParser(); }
};

/// A utility function for creating RAII objects.
template <class T, class D>
static std::unique_ptr<T, D> make_scoped(T* const ptr, D deleter)
{
    return std::unique_ptr<T, D>(ptr, deleter);
}

/// Converts a libxml UTF-8 string to our string type:
inline FPString toFPString(const xmlChar* s) { return (const char*)s; }

class OElement;  // Defined below.

/**
 * An input XML element from the DOM tree model used for file parsing.
 *
 * This is a thin wrapper class for elements of the LibXML library.
 */
class Element
{
public:
    /// Constructor that takes a LibXml element.
    Element(xmlNodePtr e = nullptr) : _el(e) {}

    /// Returns the text content of the XML element.
    FPString textContent() const;

    /// Parses a single integer parameter from a child element with the given name.
    /// Throws an exception if child element doesn't exist.
    int parseIntParameterElement(const char* tagName) const;

    /// Parses a single integer parameter from a child element.
    /// Returns the provided default value if child element doesn't exist.
    int parseOptionalIntParameterElement(const char* tagName, int defaultValue) const;

    /// Parses a single float parameter from a child element.
    /// Throws an exception if child element doesn't exist.
    double parseFloatParameterElement(const char* tagName) const;

    /// Parses a single float parameter from a child element.
    /// Returns the provided default value if child element doesn't exist.
    double parseOptionalFloatParameterElement(const char* tagName, double defaultValue) const;

    /// Parses a string value from a child element.
    /// Throws an exception if child element doesn't exist.
    FPString parseStringParameterElement(const char* tagName) const;

    /// Parses a string value from a child element.
    /// Returns the provided default value if child element doesn't exist.
    FPString parseOptionalStringParameterElement(const char* tagName, const FPString& defaultValue = FPString()) const;

    /// Parses a file path from a child element.
    /// Throws an exception if child element doesn't exist.
    FPString parsePathParameterElement(const char* tagName) const;

    /// Parses a file path from a child element.
    /// Returns the provided default value if child element doesn't exist.
    FPString parseOptionalPathParameterElement(const char* tagName, const FPString& defaultValue = FPString()) const;

    /// Parses a single float parameter from an attribute of the element.
    /// Throws an exception if attribute doesn't exist.
    double parseFloatParameterAttribute(const char* attributeName) const;

    /// Parses a single float parameter from an XML element attribute if present.
    /// Returns the provided default value if attribute doesn't exist.
    double parseOptionalFloatParameterAttribute(const char* attributeName, double defaultValue) const;

    /// Parses a single integer parameter from an XML element attribute.
    /// Throws an exception if attribute doesn't exist.
    int parseIntParameterAttribute(const char* attributeName) const;

    /// Parses a single integer parameter from an XML element attribute if present.
    /// Returns the provided default value if attribute doesn't exist.
    int parseOptionalIntParameterAttribute(const char* attributeName, int defaultValue) const;

    /// Parses a single string parameter from an XML element attribute.
    /// Throws an exception if attribute doesn't exist.
    FPString parseStringParameterAttribute(const char* attributeName) const;

    /// Parses a single string parameter from an XML element attribute if present.
    /// Returns the provided default value if attribute doesn't exist.
    FPString parseOptionalStringParameterAttribute(const char* attributeName, const FPString& defaultValue = FPString()) const;

    /// Parses a file path from an XML element attribute.
    /// Throws an exception if attribute doesn't exist.
    FPString parsePathParameterAttribute(const char* attributeName) const;

    /// Parses a single boolean parameter from an XML element attribute.
    /// Throws an exception if attribute doesn't exist.
    bool parseBooleanParameterAttribute(const char* attributeName) const;

    /// Parses a single boolean parameter from an XML element attribute if present.
    /// Returns the provided default value if attribute doesn't exist.
    bool parseOptionalBooleanParameterAttribute(const char* attributeName, bool defaultValue) const;

    /// Returns the element's first child element if it has any.
    Element firstChildElement() const;

    /// Returns the first child element with the given tag name if it has any.
    Element firstChildElement(const char* tagName) const;

    /// Returns the first child element with the given name, or throws an exception if it doesn't exist.
    Element expectChildElement(const char* tagName) const;

    /// Returns whether this element object is null (i.e. not pointing to any XML element).
    explicit operator bool() const noexcept { return _el != nullptr; }

    /// Returns true if this element's tag name equals the given string.
    bool tagEquals(const char* tagName) const;

    /// Throws an exception if the element' tag doesn't equal the given string.
    void expectTag(const char* tagName) const;

    /// Returns true if this XML element has an attribute with the given name.
    bool hasAttribute(const char* attrName) const;

    /// Returns the line number of this element in the XML file.
    int lineNumber() const;

    /// Returns the next sibling element from the children of this element's parent element.
    Element nextSibling() const;

    /// Returns the next sibling element with the given tag name from the children of this element's parent element.
    Element nextSibling(const char* tagName) const;

    /// Returns the tag name of this element.
    const char* tag() const { return (const char*)_el->name; }

    /// Returns the wrapped libxml element.
    explicit operator xmlNodePtr() const noexcept { return _el; }

    /// Creates a child element with formatted text content.
    Element createIntParameterElement(const char* tagName, int value);

    /// Creates a child element with formatted text content.
    Element createFloatParameterElement(const char* tagName, double value);

    /// Creates a child element with formatted text content.
    Element createStringParameterElement(const char* tagName, const FPString& value);

    /// Returns the number of attributes of this element.
    int attributeCount() const;

    /// Appends a child element to this element.
    void appendChild(OElement& element);

    /// Appends a child element to this element.
    void appendChild(OElement&& element);

    /// Sets an attribute on this XML element.
    void setAttribute(const char* attr, const FPString& value);

    /// Changes the tag name of this element.
    void setTagName(const FPString& tagName);

    /// Sets the text content of the element.
    /// When calling this, make sure the element does not have any child elements.
    void setTextContent(const FPString& text);

protected:
    /// Resolves a relative path or filename to an absolute path by using the
    /// base path of the XML document.
    FPString resolveFilename(const FPString& filename) const;

    /// The internal pointer to the xmllib element.
    xmlNodePtr _el;
};

/**
 * An output XML element from the DOM tree model used for file generation.
 *
 * This is a thin wrapper class for elements of the LibXML library.
 */
class OElement : public std::unique_ptr<xmlNode, void (*)(xmlNodePtr)>, public Element
{
public:
    /// Constructor that creates a null element.
    OElement() : std::unique_ptr<xmlNode, void (*)(xmlNodePtr)>(nullptr, xmlFreeNode) {}

    /// Constructor that creates an element with the given name.
    OElement(const char* name);

    /// Returns whether this element object is null (i.e. not pointing to any XML element).
    explicit operator bool() const noexcept { return _el != nullptr; }
};

}  // End of namespace
}  // End of namespace
