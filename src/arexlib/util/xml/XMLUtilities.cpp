///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "XMLUtilities.h"

namespace atomicrex {
namespace XML {

using namespace std;
using namespace boost;

#ifndef USE_BOOST_FILESYSTEM_LIB
namespace fs = std::experimental::filesystem;
#else
namespace fs = boost::filesystem;
#endif

/******************************************************************************
* Returns true if this element's tag name equals the given string.
******************************************************************************/
bool Element::tagEquals(const char* tagName) const { return !xmlStrcmp(_el->name, BAD_CAST tagName); }

/******************************************************************************
* Parses a single integer parameter from a child element with the given name.
* Throws an exception if child element doesn't exist.
******************************************************************************/
int Element::parseIntParameterElement(const char* tagName) const
{
    Element element = expectChildElement(tagName);
    try {
        return stoi(element.textContent());
    }
    catch(const std::exception&) {
        throw std::runtime_error(
            str(format("Invalid integer value in <%1%> element at line %2% of XML file.") % tagName % element.lineNumber()));
    }
}

/******************************************************************************
* Parses a single integer parameter from a child element.
* Returns the provided default value if child element doesn't exist.
******************************************************************************/
int Element::parseOptionalIntParameterElement(const char* tagName, int defaultValue) const
{
    Element element = firstChildElement(tagName);
    if(!element) return defaultValue;
    try {
        return stoi(element.textContent());
    }
    catch(const std::exception&) {
        throw std::runtime_error(
            str(format("Invalid integer value in <%1%> element at line %2% of XML file.") % tagName % element.lineNumber()));
    }
}

/******************************************************************************
* Parses a single float parameter from a child element.
* Throws an exception if child element doesn't exist.
******************************************************************************/
double Element::parseFloatParameterElement(const char* tagName) const
{
    Element element = expectChildElement(tagName);
    try {
        return stod(element.textContent());
    }
    catch(const std::exception&) {
        throw std::runtime_error(str(format("Invalid floating-point value in <%1%> element at line %2% of XML file.") % tagName %
                                     element.lineNumber()));
    }
}

/******************************************************************************
* Helper function that parses a numeric parameter from an XML element.
* If the element is not present, returns the provided default value.
******************************************************************************/
double Element::parseOptionalFloatParameterElement(const char* tagName, double defaultValue) const
{
    Element element = firstChildElement(tagName);
    if(!element) return defaultValue;
    try {
        return std::stod(element.textContent());
    }
    catch(const std::exception&) {
        throw std::runtime_error(str(format("Invalid floating-point value in <%1%> element at line %2% of XML file.") % tagName %
                                     element.lineNumber()));
    }
}

/******************************************************************************
* Parses a string value from a child element.
* Throws an exception if child element doesn't exist.
******************************************************************************/
FPString Element::parseStringParameterElement(const char* tagName) const
{
    Element element = expectChildElement(tagName);
    FPString value = element.textContent();
    boost::trim(value);
    if(value.empty())
        throw std::runtime_error(
            str(format("Invalid empty <%1%> element at line %2% of XML file.") % tagName % element.lineNumber()));
    return value;
}

/******************************************************************************
* Parses a string value from a child element.
* Returns the provided default value if child element doesn't exist.
******************************************************************************/
FPString Element::parseOptionalStringParameterElement(const char* tagName, const FPString& defaultValue) const
{
    Element element = firstChildElement(tagName);
    if(!element) return defaultValue;
    FPString value = element.textContent();
    boost::trim(value);
    if(value.empty())
        throw std::runtime_error(
            str(format("Invalid empty <%1%> element at line %2% of XML file.") % tagName % element.lineNumber()));
    return value;
}

/******************************************************************************
* Parses a file path from a child element.
* Throws an exception if child element doesn't exist.
******************************************************************************/
FPString Element::parsePathParameterElement(const char* tagName) const
{
    Element element = expectChildElement(tagName);
    FPString value = element.textContent();
    boost::trim(value);
    if(value.empty())
        throw std::runtime_error(
            str(format("Invalid empty <%1%> element at line %2% of XML file.") % tagName % element.lineNumber()));
    return resolveFilename(value);
}

/******************************************************************************
* Parses a file path from a child element.
* Returns the provided default value if child element doesn't exist.
******************************************************************************/
FPString Element::parseOptionalPathParameterElement(const char* tagName, const FPString& defaultValue) const
{
    Element element = firstChildElement(tagName);
    if(!element) return defaultValue;
    FPString value = element.textContent();
    boost::trim(value);
    if(value.empty())
        throw std::runtime_error(
            str(format("Invalid empty <%1%> element at line %2% of XML file.") % tagName % element.lineNumber()));
    return resolveFilename(value);
}

/******************************************************************************
* Parses a single float parameter from an attribute of the element.
* Throws an exception if attribute doesn't exist.
******************************************************************************/
double Element::parseFloatParameterAttribute(const char* attributeName) const
{
    FPString s = parseStringParameterAttribute(attributeName);
    try {
        return stod(s);
    }
    catch(const std::exception&) {
        throw std::runtime_error(
            str(format("Invalid floating-point value in attribute '%1%' of <%2%> element in line %3% of XML file.") %
                attributeName % tag() % lineNumber()));
    }
}

/******************************************************************************
* Parses a single float parameter from an XML element attribute if present.
* Returns the provided default value if attribute doesn't exist.
******************************************************************************/
double Element::parseOptionalFloatParameterAttribute(const char* attributeName, double defaultValue) const
{
    if(!hasAttribute(attributeName)) return defaultValue;
    return parseFloatParameterAttribute(attributeName);
}

/******************************************************************************
* Parses a single integer parameter from an XML element attribute.
* Throws an exception if attribute doesn't exist.
******************************************************************************/
int Element::parseIntParameterAttribute(const char* attributeName) const
{
    FPString s = parseStringParameterAttribute(attributeName);
    try {
        return stoi(s);
    }
    catch(const std::exception&) {
        throw std::runtime_error(
            str(format("Invalid integer value in attribute '%1%' of <%2%> element in line %3% of XML file.") % attributeName %
                tag() % lineNumber()));
    }
}

/******************************************************************************
* Parses a single integer parameter from an XML element attribute if present.
* Returns the provided default value if attribute doesn't exist.
******************************************************************************/
int Element::parseOptionalIntParameterAttribute(const char* attributeName, int defaultValue) const
{
    if(!hasAttribute(attributeName)) return defaultValue;
    return parseIntParameterAttribute(attributeName);
}

/******************************************************************************
* Parses a single boolean flag from an XML element attribute.
* Throws an exception if attribute doesn't exist.
******************************************************************************/
bool Element::parseBooleanParameterAttribute(const char* attributeName) const
{
    FPString s = parseStringParameterAttribute(attributeName);
    return (s == "true");
}

/******************************************************************************
* Parses a single integer parameter from an XML element attribute if present.
* Returns the provided default value if attribute doesn't exist.
******************************************************************************/
bool Element::parseOptionalBooleanParameterAttribute(const char* attributeName, bool defaultValue) const
{
    if(!hasAttribute(attributeName)) return defaultValue;
    return parseBooleanParameterAttribute(attributeName);
}

/******************************************************************************
* Parses a single string parameter from an XML element attribute.
* Throws an exception if attribute doesn't exist.
******************************************************************************/
FPString Element::parseStringParameterAttribute(const char* attributeName) const
{
    auto s = make_scoped(xmlGetProp(_el, BAD_CAST attributeName), xmlFree);
    if(!s)
        throw std::runtime_error(str(format("Element <%1%> is missing attribute '%2%' in line %3% of XML file.") % tag() %
                                     attributeName % lineNumber()));
    return toFPString(s.get());
}

/******************************************************************************
* Parses a single string parameter from an XML element attribute if present.
* Returns the provided default value if attribute doesn't exist.
******************************************************************************/
FPString Element::parseOptionalStringParameterAttribute(const char* attributeName, const FPString& defaultValue) const
{
    if(!hasAttribute(attributeName)) return defaultValue;
    return parseStringParameterAttribute(attributeName);
}

/******************************************************************************
* Parses a file path from an XML element attribute.
* Throws an exception if attribute doesn't exist.
******************************************************************************/
FPString Element::parsePathParameterAttribute(const char* attributeName) const
{
    auto s = make_scoped(xmlGetProp(_el, BAD_CAST attributeName), xmlFree);
    if(!s)
        throw std::runtime_error(str(format("Element <%1%> is missing attribute '%2%' in line %3% of XML file.") % tag() %
                                     attributeName % lineNumber()));
    FPString value = toFPString(s.get());
    boost::trim(value);
    if(value.empty())
        throw std::runtime_error(
            str(format("Invalid empty <%1%> element at line %2% of XML file.") % attributeName % lineNumber()));
    return resolveFilename(value);
}

/******************************************************************************
* Returns the text content of the XML element.
******************************************************************************/
FPString Element::textContent() const
{
    auto s = make_scoped(xmlNodeListGetString(_el->doc, _el->children, 1), xmlFree);
    return toFPString(s.get());
}

/******************************************************************************
* Returns the line number of this element in the XML file.
******************************************************************************/
int Element::lineNumber() const { return xmlGetLineNo(_el); }

/******************************************************************************
* Throws an exception if the element' tag doesn't equal the given string.
******************************************************************************/
void Element::expectTag(const char* tagName) const
{
    if(!tagEquals(tagName))
        throw std::runtime_error(
            str(format("Expected <%1%> element in line %2% of XML file, but found <%3%>.") % tagName % lineNumber() % tag()));
}

/******************************************************************************
* Returns the element's first child element if it has any.
******************************************************************************/
Element Element::firstChildElement() const { return xmlFirstElementChild(_el); }

/******************************************************************************
* Returns the first child element with the given tag name if it has any.
******************************************************************************/
Element Element::firstChildElement(const char* tagName) const
{
    for(xmlNodePtr element = _el->children; element != nullptr; element = xmlNextElementSibling(element)) {
        if(!xmlStrcmp(element->name, BAD_CAST tagName)) return element;
    }
    return nullptr;
}

/******************************************************************************
* Returns the first child element with the given name, or throws an exception
* if it doesn't exist.
******************************************************************************/
Element Element::expectChildElement(const char* tagName) const
{
    Element element = firstChildElement(tagName);
    if(!element)
        throw std::runtime_error(
            str(format("Expected <%1%> element in <%2%> element in line %3% of XML file.") % tagName % tag() % lineNumber()));
    return element;
}

/******************************************************************************
* Returns the next sibling element from the children of this element's parent element.
******************************************************************************/
Element Element::nextSibling() const { return xmlNextElementSibling(_el); }

/******************************************************************************
* Returns the next sibling element with the given tag namefrom the children of this element's parent element.
******************************************************************************/
Element Element::nextSibling(const char* tagName) const
{
    for(xmlNodePtr e = xmlNextElementSibling(_el); e != nullptr; e = xmlNextElementSibling(e)) {
        if(!xmlStrcmp(e->name, BAD_CAST tagName)) return e;
    }
    return Element();
}

/******************************************************************************
* Returns true if this XML element has an attribute with the given name.
******************************************************************************/
bool Element::hasAttribute(const char* attrName) const { return xmlHasProp(_el, BAD_CAST attrName); }

/******************************************************************************
* Resolves a relative path or filename to an absolute path by using the
* base path of the XML document.
******************************************************************************/
FPString Element::resolveFilename(const FPString& filename) const
{
    if(filename.empty()) return filename;
    fs::path base(toFPString(_el->doc->URL));
    base.remove_filename();
    return fs::absolute(filename, base).native();
}

/******************************************************************************
* Creates a child element with formatted text content.
******************************************************************************/
Element Element::createIntParameterElement(const char* tagName, int value)
{
    OElement child(tagName);
    child.setTextContent(to_string(value));
    appendChild(child);
    return child;
}

/******************************************************************************
* Creates a child element with formatted text content.
******************************************************************************/
Element Element::createFloatParameterElement(const char* tagName, double value)
{
    OElement child(tagName);
    child.setTextContent(to_string(value));
    appendChild(child);
    return child;
}

/******************************************************************************
* Creates a child element with formatted text content.
******************************************************************************/
Element Element::createStringParameterElement(const char* tagName, const FPString& value)
{
    OElement child(tagName);
    child.setTextContent(value);
    appendChild(child);
    return child;
}

/******************************************************************************
* Returns the number of attributes of this element.
******************************************************************************/
int Element::attributeCount() const
{
    int count = 0;
    for(xmlAttrPtr attr = _el->properties; attr; attr = attr->next) count++;
    return count;
}

/******************************************************************************
* Appends a child element to this element.
******************************************************************************/
void Element::appendChild(OElement& element) { xmlAddChild(_el, element.release()); }

/******************************************************************************
* Appends a child element to this element.
******************************************************************************/
void Element::appendChild(OElement&& element) { xmlAddChild(_el, element.release()); }

/******************************************************************************
* Sets an attribute on this XML element.
******************************************************************************/
void Element::setAttribute(const char* attr, const FPString& value) { xmlSetProp(_el, BAD_CAST attr, BAD_CAST value.c_str()); }

/******************************************************************************
* Changes the tag name of this element.
******************************************************************************/
void Element::setTagName(const FPString& tagName) { xmlNodeSetName(_el, BAD_CAST tagName.c_str()); }

/******************************************************************************
* Sets the text content of the element.
* When calling this, make sure the element does not have any child elements.
******************************************************************************/
void Element::setTextContent(const FPString& text) { xmlNodeAddContent(_el, BAD_CAST text.c_str()); }

/******************************************************************************
* Constructor that creates an element with the given name.
******************************************************************************/
OElement::OElement(const char* name) : std::unique_ptr<xmlNode, void (*)(xmlNodePtr)>(nullptr, xmlFreeNode)
{
    reset(xmlNewNode(nullptr, BAD_CAST name));
    _el = get();
}

}  // End of namespace
}  // End of namespace
