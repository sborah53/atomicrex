///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "../job/FitJob.h"
#include "../util/xml/XMLUtilities.h"
#include "PointDefectStructure.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
* Updates the structure (atom positions, simulation cell, etc.)
******************************************************************************/
void PointDefectStructure::updateStructure()
{
    if(isDirty(STRUCTURE_DOF)) {
        // Set up cell.
        setupSimulationCell(superCell());

        // Get atoms of perfect lattice.
        std::vector<std::pair<Point3, int>> atoms = superCellAtoms();

        // Create substitutional atoms.
        for(size_t n = 0; n < _substitutionalIndices.size(); n++) {
            if(_substitutionalIndices[n] < 1 || _substitutionalIndices[n] > atoms.size())
                throw runtime_error(str(format("Index for substitutional atom %1% in point defect structure '%2%' is out of "
                                               "range. Super cell has only %3% atoms.") %
                                        _substitutionalIndices[n] % id() % atoms.size()));
            atoms[_substitutionalIndices[n] - 1].second = _substitutionalTypes[n];
        }

        // Create vacancies.
        for(int v : _vacancyIndices) {
            if(v < 1 || v > atoms.size())
                throw runtime_error(str(format("Index for vacancy atom %1% in point defect structure '%2%' is out of range. "
                                               "Super cell has only %3% atoms.") %
                                        v % id() % atoms.size()));
            atoms.erase(atoms.begin() + (v - 1));
        }

        // Create interstitial atoms.
        for(int n = 0; n < _interstitialTypes.size(); n++) {
            // Interstitial positions are specified in reduced coordinates of the unit cell.
            // Need to convert to absolute coordinates here.
            atoms.push_back(std::make_pair(unitCell() * _interstitialPositions[n], _interstitialTypes[n]));
        }

        // Update structure.
        setAtomCount(atoms.size());
        auto pos = atomPositions().begin();
        auto type = atomTypes().begin();
        for(const auto& atom : atoms) {
            *pos++ = atom.first;
            *type++ = atom.second;
        }

        setDirty(ATOM_POSITIONS);

        // Structure has been created from the parameters stored in the DOFs.
        clearDirty(STRUCTURE_DOF);
    }

    // Let the base class do the rest (updating the neighbor lists etc).
    AtomicStructure::updateStructure();
}

/******************************************************************************
* Parses any structure-specific parameters in the XML element in the job file.
******************************************************************************/
void PointDefectStructure::parse(XML::Element structureElement)
{
    SuperCellStructure::parse(structureElement);
    XML::Element defectElement = structureElement.firstChildElement("defects");

    // Parse vacancies.
    for(XML::Element vacancyElement = defectElement.firstChildElement("vacancy"); vacancyElement;
        vacancyElement = vacancyElement.nextSibling("vacancy")) {
        int index = vacancyElement.parseIntParameterAttribute("index");
        _vacancyIndices.push_back(index);
    }

    // Sort vacancy index list in descending order to prevent changing the indices while deleting atoms.
    sort(_vacancyIndices.begin(), _vacancyIndices.end(), greater<int>());

    // Parse substitutionals.
    for(XML::Element substitutionalElement = defectElement.firstChildElement("substitutional"); substitutionalElement;
        substitutionalElement = substitutionalElement.nextSibling("substitutional")) {
        int index = substitutionalElement.parseIntParameterAttribute("index");
        int atomType = job()->parseAtomTypeAttribute(substitutionalElement, "atom-type");
        _substitutionalIndices.push_back(index);
        _substitutionalTypes.push_back(atomType);
    }

    // Parse interstitials.
    for(XML::Element interstitialElement = defectElement.firstChildElement("interstitial"); interstitialElement;
        interstitialElement = interstitialElement.nextSibling("interstitial")) {
        double x = interstitialElement.parseFloatParameterAttribute("x");
        double y = interstitialElement.parseFloatParameterAttribute("y");
        double z = interstitialElement.parseFloatParameterAttribute("z");
        int atomType = job()->parseAtomTypeAttribute(interstitialElement, "atom-type");
        _interstitialPositions.push_back(Point3(x, y, z));
        _interstitialTypes.push_back(atomType);
    }
}
}
