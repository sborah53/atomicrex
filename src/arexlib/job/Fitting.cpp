///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "FitJob.h"
#include "../potentials/Potential.h"
#include "../dof/DegreeOfFreedom.h"
#include "../properties/FitProperty.h"
#include "../structures/AtomicStructure.h"
#include "../minimizers/LBFGSMinimizer.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/******************************************************************************
 * Prepares a list of all degrees of freedom and properties that are to be fitted.
 ******************************************************************************/
void FitJob::prepareFitting()
{
    // Prepare all potentials for computation.
    for(Potential* potential : potentials()) potential->preparePotential();

    // Gather all degrees of freedom of the potentials.
    _completePotentialDOFList.clear();
    for(Potential* potential : potentials()) potential->listAllDOF(_completePotentialDOFList);

    // Collect all unreferenced structures in one list.
    _unreferencedStructures.clear();
    for(AtomicStructure* structure : structures()) {
        if(std::find(referencedStructures().begin(), referencedStructures().end(), structure) == referencedStructures().end()) {
            addUnreferencedStructure(structure);
        }
    }

    // Gather DOFs which are enabled for fitting.
    // Also ensure that all parameters are within the specified bounds.
    // Otherwise errors can occur during the fitting process that are difficult to trace.
    _numActiveDOF = 0;
    _activePotentialDOFList.clear();
    for(DegreeOfFreedom* dof : _completePotentialDOFList) {
        if(dof->fitEnabled()) {
            _activePotentialDOFList.push_back(dof);
            _numActiveDOF += dof->numScalars();
            if(!dof->satisfiesBoundConstraints())
                throw runtime_error(str(format("Input value of parameter '%1%' does not satisfy bounds.") % dof->id()));
        }
    }

    // Assigns absolute weights to the structure and potential fit properties.
    MsgLogger(debug) << "Determining absolute fitting weights." << endl;
    assignAbsoluteWeights();

    // Generate list of all fit properties.
    vector<FitProperty*> completePropertyList;
    for(AtomicStructure* structure : structures()) structure->listAllProperties(completePropertyList);
    for(Potential* potential : potentials()) potential->listAllProperties(completePropertyList);
    for(DerivedProperty* derivedProperty : derivedProperties()) completePropertyList.push_back(derivedProperty);

    // Gather properties which are enabled for fitting.
    _propertyList.clear();
    for(FitProperty* prop : completePropertyList) {
        if(prop->fitEnabled()) {
            _propertyList.push_back(prop);
        }
    }
}

/******************************************************************************
 * Performs the actual fitting by minimizing the residual.
 ******************************************************************************/
void FitJob::performFitting()
{
    if(!_fitMinimizer)
        throw runtime_error(
            "Cannot minimize without a minimizer.\n"
            "Please set up a minimizer in the input file.");

    BOOST_ASSERT(fittingEnabled());
    BOOST_ASSERT(_fitMinimizer);

    // Gather list of DOFs that are to be optimized.
    prepareFitting();

    // Report lists of all DOFs and properties being fitted.
    printPotentialDOFList(true);
    printFittingProperties();

    if(numActiveDOF() == 0 || _propertyList.empty()) return;  // Nothing to fit.

    // Compile linear list of bound constraints from DOFs.
    vector<Minimizer::BoundConstraints> constraintTypes(numActiveDOF());
    vector<double> lowerBounds(numActiveDOF());
    vector<double> upperBounds(numActiveDOF());
    vector<Minimizer::BoundConstraints>::iterator type_iter = constraintTypes.begin();
    vector<double>::iterator lower_iter = lowerBounds.begin();
    vector<double>::iterator upper_iter = upperBounds.begin();
    for(DegreeOfFreedom* dof : _activePotentialDOFList) dof->getBoundConstraints(type_iter, lower_iter, upper_iter);
    BOOST_ASSERT(type_iter == constraintTypes.end());
    BOOST_ASSERT(lower_iter == lowerBounds.end());
    BOOST_ASSERT(upper_iter == upperBounds.end());

    // Initialize state.
    vector<double> x0(numActiveDOF());
    packDOF(x0);

    // Define the objective function.
    auto function = [this](const vector<double>& x) -> double {
        // Unpack values from the linear vector and store them in the DOF objects.
        unpackDOF(x);
        // Perform the actual calculation of the residual, i.e. deviation from the target property values.
        return calculateResidual();
    };

    // Initialize minimizer.
    _fitMinimizer->prepare(std::move(x0), function);
    _fitMinimizer->setConstraints(std::move(constraintTypes), std::move(lowerBounds), std::move(upperBounds));

    // Minimization loop.
    Minimizer::MinimizerResult result;
    while((result = _fitMinimizer->iterate()) == Minimizer::MINIMIZATION_CONTINUE &&
          _fitMinimizer->itercount() <= _fitMinimizer->maximumNumberOfIterations()) {
        // Check if stop file has been created.
        ifstream stopFile("stop_fitpot");
        if(stopFile.is_open()) {
            MsgLogger(medium) << "Found 'stop_fitpot' file. Minimization will be terminated." << endl;
            break;
        }
    }
    if(result == Minimizer::MINIMIZATION_ERROR)
        throw runtime_error("The minimizer failed with an error while minimizing the objective function.");

    // Output new values of DOFs.
    unpackDOF(_fitMinimizer->bestParameters());
    printPotentialDOFList(false);

    MsgLogger(medium) << "Fitting process complete." << endl;
}

/******************************************************************************
 * Output current values of the DOFs.
 ******************************************************************************/
void FitJob::printPotentialDOFList(bool printFullDOFList)
{
    MsgLogger(medium) << separatorLine;
    MsgLogger(medium) << "Potential parameters being optimized (#dof=" << numActiveDOF() << "):" << endl;
    for(DegreeOfFreedom* dof : _activePotentialDOFList) {
        MsgLogger logger(medium);
        logger << "   ";
        dof->print(logger);
        logger << endl;
    }
    if(_activePotentialDOFList.empty()) MsgLogger(medium) << "   NONE" << endl;
    MsgLogger(medium) << separatorLine;

    if(printFullDOFList) {
        // Print values of parameters that have been provided but are not modified.
        int n = 0;
        MsgLogger(maximum) << "Fixed potential parameters:" << endl;
        for(DegreeOfFreedom* dof : _completePotentialDOFList) {
            if(!dof->fitEnabled()) {
                n++;
                MsgLogger logger(maximum);
                logger << "   ";
                dof->print(logger);
                logger << endl;
            }
        }
        if(n == 0) MsgLogger(maximum) << "   NONE" << endl;
        MsgLogger(maximum) << separatorLine;
    }
}

/******************************************************************************
 * Outputs the list of properties being fitted.
 ******************************************************************************/
void FitJob::printFittingProperties()
{
    // Gather properties which are enabled for fitting.
    MsgLogger(medium) << separatorLine;
    MsgLogger(maximum) << "Atomic structure properties being fitted:" << endl;
    for(FitProperty* prop : _propertyList) {
        MsgLogger(maximum) << "   " << prop->parent()->id() << "   " << prop->id() << "   relWeight " << prop->relativeWeight()
                           << "   absWeight " << prop->absoluteWeight() << endl;
    }
    MsgLogger(medium) << "Number of properties being fitted: " << _propertyList.size() << endl;
    MsgLogger(medium) << separatorLine;
}

/******************************************************************************
 * Packs the values of all degrees of freedom into a linear vector which can
 * be passed to the minimizer routine.
 ******************************************************************************/
void FitJob::packDOF(double* destination)
{
    for(DegreeOfFreedom* dof : _activePotentialDOFList) {
        dof->exportValue(destination);
    }
}

/******************************************************************************
 * Unpacks the values of all degrees of freedom from a linear vector.
 ******************************************************************************/
void FitJob::unpackDOF(const double* source)
{
    for(DegreeOfFreedom* dof : _activePotentialDOFList) {
        dof->importValue(source);
    }
}

/******************************************************************************
 * Calculates the total residual, that is the objective function to be
 * minimized during fitting. This is the weighted sum of the deviations of all
 * properties from their target values.
 ******************************************************************************/
double FitJob::calculateResidual(FitProperty::ResidualNorm norm)
{
    // Determines if we should abort calculation of the residual.
    // becomes true if a property of one structure is nan.
    bool abort = false;

    // First compute all structures that are referenced by another structure
    for(AtomicStructure* structure : referencedStructures()) {
        if(!abort) {
            abort = !structure->computeProperties(true);
        }
        else {
            MsgLogger(debug) << "Computation of  referenced structure " << structure->id() << " aborted." << endl;
            return std::numeric_limits<double>::infinity();
        }
    }

// Then compute all unreferenced structures.
#pragma omp parallel for schedule(dynamic, 1)
    for(size_t i = 0; i < unreferencedStructures().size(); i++) {
#pragma omp flush(abort)
        if(!abort) {
            abort = !unreferencedStructures()[i]->computeProperties(true);
        }
        else {
            MsgLogger(debug) << "Computation of structure " << unreferencedStructures()[i]->id() << " aborted." << endl;
        }
    }

    // return inf if the calculation was aborted.
    if(abort) {
        return std::numeric_limits<double>::infinity();
    }

    // Sum residuals of all properties included in the fit.
    double totalResidual = 0;
    for(FitProperty* prop : _propertyList) {
        BOOST_ASSERT(prop->fitEnabled());

        // Compute the property now, if it hasn't been computed by the structure or potential to which it belongs.
        prop->compute();

        double residual = prop->residual(norm);
        double absWeight = prop->absoluteWeight();

        // This provides very extensive information that can be very useful to adjust the weights properly.
        MsgLogger(debug) << " relResidual " << residual * absWeight << " absResidual " << residual << " absWeight " << absWeight
                         << " prop " << prop->parent()->id() << "   " << prop->id() << endl;

        // Check if the calculation yielded a valid result.
        if(!std::isfinite(residual)) {
            MsgLogger(debug) << "Residual of property " << prop->id() << " of structure " << prop->parent()->id()
                             << " has value <" << residual << ">. Stopping calculation of residual." << endl;
            return std::numeric_limits<double>::infinity();
        }

        // Add weighted residual to the sum.
        totalResidual += residual * absWeight;
    }

    // This line complements the information from above.
    MsgLogger(debug) << "totResidual " << totalResidual << endl << endl;

    return totalResidual;
}
}  // namespace atomicrex
