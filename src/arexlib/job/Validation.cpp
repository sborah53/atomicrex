///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Alexander Stukowski and Paul Erhart
//
//  This file is part of atomicrex.
//
//  Atomicrex is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  Atomicrex is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "FitJob.h"
#include "../potentials/Potential.h"
#include "../structures/AtomicStructure.h"
#include "../util/NumericalDerivative.h"

namespace atomicrex {

using namespace std;
using namespace boost;

/**
 * Helper class that computes the force on an atom using numerical
 * differentiation of the total energy with respect to small displacements
 * of the atom.
 */
class NumericalGradientEvaluator : public NumericalDerivative
{
public:
    /// Constructor.
    ///
    /// The atomIndex parameter selects the atom in the structure to displace.
    ///
    /// The dimension parameter selects the direction of displacement:
    /// For dimension=0,1,2 the atom position is perturbed in the X,Y,Z direction respectively.
    /// For dimension=3,4,5 the atom's (virtual) displacement vector is perturbed in the X,Y,Z direction respectively.
    NumericalGradientEvaluator(AtomicStructure& structure, int atomIndex, int dimension)
        : _structure(structure), _atomIndex(atomIndex), _dimension(dimension)
    {
    }

protected:
    /// Displaces the selected atom and computes the total energy of the atomic structure.
    virtual double evaluate(double x)
    {
        // Check if we actually displace the atom.
        if(_dimension < 3) {
            // Store old position.
            double oldp = _structure.atomPositions()[_atomIndex][_dimension];
            // Displace atom.
            _structure.atomPositions()[_atomIndex][_dimension] += x;
            _structure.setDirty(AtomicStructure::ATOM_POSITIONS);
            // Compute energy.
            double e = _structure.computeEnergy(false, false, true);
            // Restore original atom position.
            _structure.atomPositions()[_atomIndex][_dimension] = oldp;
            _structure.setDirty(AtomicStructure::ATOM_POSITIONS);
            return e;
        }
        else {
            // Only the atom's internal displacement vector is changed.
            int dim = _dimension - 3;
            BOOST_ASSERT(dim < 3);
            // Store old displacement.
            double oldp = _structure.atomDisplacements()[_atomIndex][dim];
            // Change displacement of atom.
            _structure.atomDisplacements()[_atomIndex][dim] += x;
            _structure.setDirty(AtomicStructure::ATOM_POSITIONS);
            // Compute energy.
            double e = _structure.computeEnergy(false, false, true);
            // Restore original atom displacement.
            _structure.atomDisplacements()[_atomIndex][dim] = oldp;
            _structure.setDirty(AtomicStructure::ATOM_POSITIONS);
            return e;
        }
    }

private:
    AtomicStructure& _structure;
    int _atomIndex;
    int _dimension;
};

/******************************************************************************
* Performs a check of the energy/force calculation routines using
* numerical differentiation.
******************************************************************************/
void FitJob::validatePotentials()
{
    MsgLogger(medium) << separatorLine;
    MsgLogger(medium) << "Validating potential routines." << endl;

    // Prepare all potentials for the following calculation.
    for(Potential* potential : potentials()) potential->preparePotential();

    // The predefined magnitude of the displacement applied to each atomic position:
    double epsilon = 1e-4;
    MsgLogger(maximum) << "Finite difference epsilon: " << epsilon << endl;

    // Do the test for every structure with more than one atom.
    int nstructs = 0;
    for(AtomicStructure* structure : structures()) {
        if(structure->numLocalAtoms() <= 1) continue;
        nstructs++;
        MsgLogger(maximum) << "Testing structure '" << structure->id() << "' with " << (structure->numLocalAtoms() * 3)
                           << " atomic degrees of freedom." << endl;

        // Calculate analytical forces.
        double totalEnergyWithForces = structure->computeEnergy(true, false, true);
        vector<Vector3> analyticalForces = structure->atomForces();
        vector<Point3> positions = structure->atomPositions();

        // Check results of both energy calculation routines (with/without forces).
        double totalEnergyWithoutForces = structure->computeEnergy(false, false, true);
        if(std::abs(totalEnergyWithForces - totalEnergyWithoutForces) > FLOATTYPE_EPSILON) {
            throw runtime_error(
                "Total energy mismatch. Potential routines with and without force calculation yield different total energy.");
        }

        // Compute energy derivative with respect to a displacement of each atom in each spatial direction.
        for(int a = 0; a < structure->numLocalAtoms(); a++) {
            for(int dim = 0; dim < 3; dim++) {
                double numericalDerivativeError;
                double numericalDerivative =
                    NumericalGradientEvaluator(*structure, a, dim).compute(0, numericalDerivativeError, epsilon);

                if(std::abs(numericalDerivativeError) < FLOATTYPE_EPSILON) numericalDerivativeError = FLOATTYPE_EPSILON;

                double analyticalDerivative = -analyticalForces[a][dim];
                double relativeDeviation = fabs(analyticalDerivative - numericalDerivative) / numericalDerivativeError;

                MsgLogger(maximum) << "Atom " << a << " pos" << positions[a] << " dim=" << dim
                                   << "   Numerical derivative: " << numericalDerivative << " err: " << numericalDerivativeError
                                   << "   Analytical derivative: " << analyticalDerivative
                                   << "   Deviation: " << fabs(analyticalDerivative - numericalDerivative) << "  "
                                   << relativeDeviation << endl;

                // Compare analytical derivative to numerical derivative.
                // Difference should be smaller than the error bound estimated for the numerical derivative.
                if(std::abs(numericalDerivative - analyticalDerivative) > numericalDerivativeError * 2.0)
                    throw runtime_error(str(format("Numerical derivative deviates too much from the analytical energy gradient "
                                                   "computed by the potentials for structure %1%") %
                                            structure->id()));
            }
        }
    }

    if(nstructs == 0)
        throw runtime_error(
            "Job does not contain an atomic structure with more than one atom. Cannot perform potential validation.");

    MsgLogger(medium) << "Validation successful." << endl;
    MsgLogger(medium) << separatorLine;
}
}
